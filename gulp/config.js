module.exports = {
	appFolder: './app',
	distFolder: './build',
	watchPaths: {
		css: './build/assets/css/**/*.css',
		js: './build/assets/js**/*.js',
		handlebars: './build/views/**/*.handlebars'
	},
	destFolders: {
		build: './build',
		css: './build/assets/css',
		js: './build/assets/js',
		html: './build/views',
		handlebars: './build/views',
		images: './build/assets/images' 
	},
	filePaths: {
		index: './app/index.html',
		sass: 'app/assets/sass/**/*.scss',
		js: ['app/assets/js/**/*.js','!app/assets/js/vendor/angular.min.js'],
		copyAngular:'app/assets/js/vendor/angular.min.js',
		html: './app/views/**/*.html',
		handlebars: 'app/views/**/*.handlebars',
		images: 'app/assets/images/**/*'
	},
	express:{
		file:'app.js',
		routeFolder: 'routes/*.js',
		modelFolder: 'models/*.js',
	}
};