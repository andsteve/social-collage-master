'use strict';
// gulpfile.js 
var gulp   = require('gulp');
var config = require('../config');
 
gulp.task('watches', function () {
	gulp.watch(config.filePaths.sass, ['sass']);
	gulp.watch(config.filePaths.js, ['minify_js']);
	gulp.watch(config.filePaths.index, ['copyIndex']);
	gulp.watch(config.filePaths.handlebars, ['copyHandlebars']);
});