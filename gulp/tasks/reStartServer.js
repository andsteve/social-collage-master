'use strict';

// gulpfile.js 
var gulp   = require('gulp');
var server = require('gulp-express');


var config = require('../config');
 
gulp.task('reStartServer', function () {

	gulp.watch(config.filePaths.sass, ['sass']);
	gulp.watch(config.filePaths.js, ['minify_js']);
	gulp.watch(config.filePaths.index, ['copyIndex']);
	gulp.watch(config.filePaths.handlebars, ['copyHandlebars']);
	gulp.watch(config.express.file, ['reStartServer']);
	gulp.watch(config.express.routeFolder, ['reStartServer']);

	server.run([config.express.file]);
});