'use strict';

// requires
var gulp = require('gulp');

// config file
var config = require('../config');

/* this task copies the handlebar files on app folder to build folder */
gulp.task('copyAngular', function() { 

	return gulp.src(config.filePaths.copyAngular)
		.pipe(gulp.dest(config.destFolders.js));
});