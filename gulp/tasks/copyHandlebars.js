'use strict';

var gulp = require('gulp');
var config = require('../config');

//Copy task, move any file in here
gulp.task('copyHandlebars', function() { 
	return gulp.src(config.filePaths.handlebars)
		.pipe(gulp.dest(config.destFolders.handlebars));
});