(function() {

	'use strict';
	// angular.module('myapp', []);

	angular
		.module('myapp', []);

	angular.module('myapp').config(function($interpolateProvider) {
		$interpolateProvider.startSymbol('{[{');
		$interpolateProvider.endSymbol('}]}');
	});

})();




