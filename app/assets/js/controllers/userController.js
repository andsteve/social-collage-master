(function () {
	'use strict';
	angular
	.module('myapp')
	.controller('userController',user);

	user.$inject = [
		'$scope',
		'$http'
	];

	function user($scope,$http){
		
		var profileMember = document.getElementsByClassName("profileMember")[0];

		/* personal info */
		var editOpcion = profileMember.getElementsByClassName("edit-opcions")[0];
		var editPersonalInfo = editOpcion.getElementsByClassName("edit-personal-info")[0];
		var editSocialNetworks = editOpcion.getElementsByClassName("edit-social-networks")[0];
		
		/* modal to add new socal network */
		var socialNetworkModal = profileMember.getElementsByClassName("new-social-network-modal")[0];
		var socialNetWorkToAdd = socialNetworkModal.getElementsByClassName("social-network-to-add")[0];
		var addSocialNetworks = editOpcion.getElementsByClassName("add-social-networks")[0];
		// var assignNewBrand = editOpcion.getElementsByClassName("assign-new-brand")[0];
		
		/* social networks info */
		var editModal = profileMember.getElementsByClassName("editModal")[0];
		var personalInfoToEdit = editModal.getElementsByClassName("personal-info-to-edit")[0];
		var socialNetworksToEdit = editModal.getElementsByClassName("social-networks-to-edit")[0];

		/* avatar info */
		var imgContainer = profileMember.getElementsByClassName("img-container")[0];
		var avatarToEdit = editModal.getElementsByClassName("avatar-to-edit")[0];
		var imgAvatar = imgContainer.getElementsByTagName("img")[0];
		
		/* event click to show personal information modal */
		editPersonalInfo.addEventListener("click", function(){
			editModal.style.display = "block";
			personalInfoToEdit.style.display = "block";
		});

		/* event click to show social networks modal to edit or delete */
		editSocialNetworks.addEventListener("click", function(){
			editModal.style.display = "block";
			socialNetworksToEdit.style.display = "block";
		});


		var userRole = profileMember.getElementsByClassName('user-role');


	    for (var i = 0; i < userRole.length; i++) {
			userRole[i].addEventListener("click", changeUserRole);
		}


		function changeUserRole(event) {

			/* get the id of the url */
			var url = window.location.pathname;
			var id = url.substring(url.lastIndexOf('/') + 1);

			var data={
				'id_user':id,
				'id_role':event.target.value
			}
			
			$http({
				method: 'POST',
				url: '/userbrands/updateRole',
				data: data
			}).then(function successCallback(response) {

			}, function errorCallback(response) {

			});
		}

		/* event click to show the modal to add new social network for an user */
		addSocialNetworks.addEventListener("click", function(){

			socialNetworkModal.style.display = "block";
			socialNetWorkToAdd.style.display = "block";

			/* request to the serve to bring all social networks and load the select */
			$http({
				method: 'GET',
				url: '/getSocialNetwork',
				data:{}
			}).then(function successCallback(response) {
				/* if the user is not authenticated render to admin/login */
				if(response.data.auth){
					window.location = "/admin/login";
				}else{
					/* load the modal containar */
					$scope.socialNeworks = response.data;
				}

			}, function errorCallback(response) {

			});

		});



		/* event click to show the modal to edit the avatar */
		imgAvatar.addEventListener("click", function(){
			editModal.style.display = "block";
			avatarToEdit.style.display = "block";
		});

		/* function to add new social network for an user */
		$scope.addSocialNetwork = function(id_user){
			
			var message = socialNetworkModal.getElementsByClassName("message")[0];

			/* checks if the user name and social selected is not empty */
			if ($scope.userName && $scope.socialSelected) {
				
				/* if the there is a message remove the message*/
				if(message){
					message.style.display = "none";
				}
				/* create the object users_networks to send on the server */
				var users_networks = {
					'id_users' : id_user,
					'social_networks_id_social_networks': $scope.socialSelected,
					'user_name' : $scope.userName
				};

				/* make a request add new user network*/
				$http({
					method: 'POST',
					url: '/addUsersNetwork',
					data:users_networks
				}).then(function successCallback(response) {

					/* if the user is not authenticated render to admin/login */
					if(response.data.auth){
						window.location = "/admin/login";
					}else{

						if(response.data.msg){	
							message.innerHTML = response.data.msg;
							message.style.display = "block";

							/* add the class warning */
							message.classList.add("warning");
						}else{

							// var socialNetwork = response.data[0];
							// var infoContainer = profileMember.getElementsByClassName("info-container")[0];
							// var socialContainer = infoContainer.getElementsByClassName("social-container")[0];

							// // /* create the icon social network */
							// var icon = createIconSocialNetwork(socialNetwork);

							// icon.style.marginRight = "4px";
							// // /* append it to the social container */
							// socialContainer.appendChild(icon);

													/* get the id of the url */
							// var url = window.location.pathname;
							// var id = url.substring(url.lastIndexOf('/') + 1);
							window.location = '/user/'+id_user;

							/* clear the input and the select */
							$scope.socialSelected = '';
							$scope.userName = '';

							/* clase the modal */
							$scope.cancel();
						}
					}

				}, function errorCallback(response) {

				});

			}else{
				/* display a a warning message */
				message.innerHTML = "The social network or user name is empty!";
				message.style.display = "block";

				/* add the class warning */
				message.classList.add("warning");
			}
		}

		/* function to update the personal information */
		$scope.update = function(){

			$scope.user = {};

			/* the all inputs form the from and load the values */
			var formImputs = personalInfoToEdit.getElementsByClassName("inputsForm");

			/* get the values of the form */
			$scope.user.name = formImputs[0].value;
			$scope.user.lastname = formImputs[1].value;
			$scope.user.id_users = formImputs[2].value;

			/* make a request to update personal information */
			$http({
				method: 'POST',
				url: '/user/update',
				data:$scope.user
			}).then(function successCallback(response) {
				/* if the user is not authenticated render to admin/login */
				if(response.data.auth){
					window.location = "/admin/login";
				}else{
					/* load the success message from the server */
					var msgUpdte = response.data.message.msg;

					/* load the personal information of the user */
					var user = response.data.user[0]

					/* get the label message */
					var message = profileMember.getElementsByClassName("message")[0];

					/* load the label message with the data */
					message.innerHTML = msgUpdte;
					message.style.display = "block";

					/* add the success class to de label */
					message.classList.add("success");

					/* close modal */
					$scope.cancel();

					/* get the tag that has the personal information */
					var infoContainer = profileMember.getElementsByClassName("info-container")[0];
					var userDetails = infoContainer.getElementsByClassName("user-details")[0];
					var spans = userDetails.getElementsByTagName("span");

					/* update personal information */
					spans[0].innerHTML = user.name;
					spans[1].innerHTML = user.lastname;
					
					/* show the message success for 3 seconds */
					setTimeout(function(){
		         		message.style.display = "none";
		         	},3000);
	         	}

			}, function errorCallback(response) {

			});

		}

		/* function to close the modals */
		$scope.cancel = function(){

			/* close the personal information modal */
			editModal.style.display = "none";
			personalInfoToEdit.style.display = "none";
			
			/* close the social network to edit modal */
			socialNetworksToEdit.style.display = "none";
			
			/* close the avatar to edit modal */
			avatarToEdit.style.display = "none";

			/* close the add user social network modal */
			socialNetworkModal.style.display = "none";
			socialNetWorkToAdd.style.display = "none";

			return false;
		}

		/* function to edit social networks */
		$scope.editSocial = function(id_user,id_social){
			
			/* get the row that has the user that we want to edit */
			var tr = document.getElementById(id_social);
			var input = tr.getElementsByTagName('input')[0];

			/* get the value of the input */
			var user_name = input.value;

			/* create an object to the new information to edit */
			var userNetwork={
				"id_user" : id_user,
				"id_social" : id_social,
				"user_name" : user_name
			}

			/* make a request to edit the user name of the the specific social network */
			$http({
				method: 'POST',
				url: '/userNetwork/update',
				data:userNetwork
			}).then(function successCallback(response) {
				/* if the user is not authenticated render to admin/login */
				if(response.data.auth){
					window.location = "/admin/login";
				}else{
					/* load the success message from the server */
					var msgUpdte = response.data.message.msg;

					/* get the label message */
					var message = socialNetworksToEdit.getElementsByClassName("message")[0];

					/* load the label message with the data */
					message.innerHTML = msgUpdte;
					message.style.display = "block";

					/* add the success class to de label */
					message.classList.add("success");
					
					/* show the message success for 3 seconds */
					setTimeout(function(){
						message.style.display = "none";
					},3000);
				}
			}, function errorCallback(response) {

			});
		}

		/* function to delete social network */
		$scope.deleteSocial = function(id_user,id_social){
			
			/* get the row that has the user that we want to delete */
			var tr = document.getElementById(id_social);

			/* create an object with the information to delete */
			var userNetwork={
				"id_user" : id_user,
				"id_social" : id_social
			}

			/* make a request to delete the userNetwork */
			$http({
				method: 'POST',
				url: '/userNetwork/delete',
				data:userNetwork
			}).then(function successCallback(response) {
				/* if the user is not authenticated render to admin/login */
				if(response.data.auth){
					window.location = "/admin/login";
				}else{
					/* remove the row of the table */
					tr.remove();

					/* load the success message from the server */
					var msgUpdte = response.data.msg;

					/* get the tag with the social container */
					var info_container = profileMember.getElementsByClassName('info-container')[0];
					var social_container = info_container.getElementsByClassName('social-container')[0];
					var icon_href = social_container.getElementsByClassName(id_social)[0];
					
					/* remove from social container the social user social network */
					icon_href.remove();

					/* get the label message */
					var message = socialNetworksToEdit.getElementsByClassName("message")[0];

					/* load the label message with the data */
					message.innerHTML = msgUpdte;
					message.style.display = "block";

					/* add the success class to de label */
					message.classList.add("success");
					
					/* show the message success for 3 seconds */
					setTimeout(function(){
		         		message.style.display = "none";
		         	},3000);
				}

			}, function errorCallback(response) {

			});
		}

		/* function to update avatar */
		$scope.updateAvatar = function(){
			// imgAvatar.src ="";
			var formData = new FormData;
		
			// get the input file and appends to the formData
			var file = document.getElementById('avatar').files[0];
			var id = document.getElementById('id');
			var number = document.getElementById('number');
			var exAvatar = document.getElementById('exAvatar');

			/* load the formData with the inputs values */
			formData.append("exAvatar", exAvatar.value);
			formData.append("number", number.value);
			formData.append("id_user", id.value);
			formData.append("avatar", file);

			/* make a request to the avatar of the user */
			$http.post('/user/updateAvatar',formData,{
				transformRequest: angular.identity,
				headers:{
					'Content-Type': undefined
				}
			}).then(function(res){
				/* if the user is not authenticated render to admin/login */
				if(res.data.auth){
					window.location = "/admin/login";
				}else{
					/* load the new user with the new avatar */
					var user = res.data[0];
					
					/* get the label message */
					var message = profileMember.getElementsByClassName("message")[0];

					/* update the image */
					imgAvatar.src = user.avatar;
					
					/* get the inputs form */
					var formImputs = avatarToEdit.getElementsByClassName("inputsForm")[0];
					
					/* clear the input */
					formImputs.value = '';

					/* close the modal */
					$scope.cancel();

					/* diplay message */
					message.innerHTML = user.message.msg;
					message.style.display = "block";

					/* add the success class to de label */
					message.classList.add("success");

					/* show the message success for 3 seconds */
					setTimeout(function(){
		         		message.style.display = "none";
		         	},3000);
				}
			});	
		}

		/*
		* this function create a new icon social network
		*/
		function createIconSocialNetwork(socialNetwork){

			var a = document.createElement("A");
			var span = document.createElement("span");
			var img = document.createElement("IMG");

			a.classList.add(socialNetwork.social_networks_id_social_networks);

			a.target = "_blank";
			a.href="";

			span.classList.add("icon");
			img.src = socialNetwork.image;

			span.appendChild(img);
			a.appendChild(span);

			return a;
		}

	}
})();