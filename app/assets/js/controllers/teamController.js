(function () {
	'use strict';
	angular
	.module('myapp')
	.controller('teamController',team);

	team.$inject = [
		'$scope',
		'$http'
	];

	function team($scope,$http){
		
		var vm = this;
		$scope.user={};
		var team = document.getElementsByClassName('team')[0];
		var modal = team.getElementsByClassName('modal')[0];
		var cancel = modal.getElementsByClassName("cancel")[0];

		var brandStatus = team.getElementsByClassName('brandStatus')[0];


		brandStatus.addEventListener("click", changeBrandStatus);

		function changeBrandStatus(event) {

			var data={
				'active':event.target.checked,
				'id_brands':event.target.id
			}
			
			$http({
				method: 'POST',
				url: '/brand/updateStatus',
				data: data
			}).then(function successCallback(response) {

			}, function errorCallback(response) {

			});

		}

		/* This function enables the bottons to edit and delete a member */
		$scope.enable = function(id){

			var data={
				'active':event.target.checked,
				'id_users':id
			}

			$http({
				method: 'POST',
				url: '/user/updateStatus',
				data: data
			}).then(function successCallback(response) {

			}, function errorCallback(response) {

			});
		}
		
		/* This function display the modal to add a new member */
		$scope.showModalAddMember = function(){
			modal.style.display = "block";
		}

		/* This function cancels the modal and clean all the inputs fields */
		cancel.onclick = function() {
			var formImputs = modal.getElementsByClassName("inputsForm");
			var alert = modal.getElementsByClassName('alert-danger')[0];
			
			if(alert){
				alert.style.display = "none";
			}
			
			for (var i = 0; i < formImputs.length; i++) {
				formImputs[i].value = "";
			}

			modal.style.display = "none";
			return false;
		}


		$scope.submit = function() {
			//create new form
			var formData = new FormData;
			// it appends all the inputs data to the formData
			for(key in $scope.user){
				formData.append(key, $scope.user[key]);
			}

			// get the input file and appends to the formData
			var file = document.getElementById('avatar').files[0];
			var id = document.getElementById('id');

			formData.append("brand", id.value);
			formData.append("avatar", file);

			/* request to save the new team */
			$http.post('/team/add',formData,{
				transformRequest: angular.identity,
				headers:{
					'Content-Type': undefined
				}
			}).then(function(res){
				if(res.data.auth){
					window.location = "/admin/login";
				}else{

					window.location = "/team/" + res.data.brand;
				}
			});		
		}

		$scope.deleteMember = function(id){

			var array = event.target.className.split('delete');
			
			if(array[1] != " disable"){
				var deleteMemberModal = team.getElementsByClassName('deleteMemberModal')[0];
				
				deleteMemberModal.style.display = "block";
				
				/**
				* Deleted member
				*/
				$scope.btnDelete = function(){
					$http({
						method: 'POST',
						url: '/user/deleteUser',
						data:{'id':id}
					}).then(function successCallback(response) {
						
						var tr = document.getElementById(id);
						tr.remove();
						var message = document.getElementById("message");
						message.innerHTML=response.data.msg;
						message.style.display = "block";
						deleteMemberModal.style.display = "none";
						
						setTimeout(function(){
			         		document.getElementById("message").remove();
			         	},3000);
			         	deleteMemberModal.style.display = "none";

					}, function errorCallback(response) {

					});
				}
				$scope.btnCancel = function(){
					deleteMemberModal.style.display = "none";
					return false;
				}
			}
		}

		$scope.editMember = function(id){
			var array = event.target.className.split('edit');

			if(array[0] == "disable" || array[1] == " disable"){
				event.preventDefault();
				return false;
			}
		}
	}
})();	