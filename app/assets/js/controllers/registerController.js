(function () {
	'use strict';

	angular
	.module('myapp')
	.controller('registerController',register);

	register.$inject = [
		'$scope',
		'$http'
	];

	function register($scope,$http){	
		

		document.getElementById("avatar").onchange = function () {
			var reader = new FileReader();
			reader.onload = function (e) {
				var avatar_selected = document.getElementsByClassName("avatar-selected")[0];
				var img = avatar_selected.getElementsByTagName("img")[0];

				// get loaded data and render thumbnail.
				img.src = e.target.result;

				avatar_selected.style.display = "block";
			};

			// read the image file as a data URL.
			reader.readAsDataURL(this.files[0]);
		};

		$scope.register = function(){
			var message = document.getElementsByClassName("message")[0];

			/* get the id of the url */
			var url = window.location.pathname;
			var id = url.substring(url.lastIndexOf('/') + 1);

			//create new form
			var formData = new FormData;
			
			// it appends all the inputs data to the formData
			$scope.user.id_brand = id;
			
			for(key in $scope.user){
				formData.append(key, $scope.user[key]);
			}

			// get the input file and appends to the formData
			var file = document.getElementById('avatar').files[0];

			formData.append("avatar", file);

			/* request to save the new team */
			$http.post('/register',formData,{
				transformRequest: angular.identity,
				headers:{
					'Content-Type': undefined
				}
			}).then(function(res){

				/* get the inputs form */
				var formImputs = document.getElementsByClassName("inputsForm")[3];
				
				/* show error message the user alreay exist */
				if(res.data.error){

					message.innerHTML= res.data.error;
					message.classList.add("warning");
					message.style.display = "block";

				}else{

					$scope.user = {};
						
					formImputs.value = '';
					
					window.location = "/";
				}
				
			});	
		}
	}

})();