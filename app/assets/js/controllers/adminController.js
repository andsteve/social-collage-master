(function() {
	'use strict';

	angular
		.module('myapp')
		.controller('adminController',admin);

	admin.$inject = [
		'$scope',
		'$http'
	];
	
	
	function admin($scope, $http){
		
		var vm = this;
		$scope.account={};
		$scope.user = {};
		var modal = document.getElementById('myModal');

		/* open he model to add a new account*/
		$scope.modalEvent = function(){

			// Get the button that opens the modal
			var newBigAccount = document.getElementsByClassName('newBigAccount')[0];
			var button = newBigAccount.getElementsByTagName('button')[0];

			// button to close the modal
			var cancel = modal.getElementsByClassName("cancel")[0];

			// When the user clicks on the button, open the modal 
			button.onclick = function(e) {
				modal.style.display = "block";
			}

			// function to close the modal
			cancel.onclick = function() {
				var formImputs = modal.getElementsByClassName("inputsForm");
				var alert = modal.getElementsByClassName('alert-danger')[0];
				
				if(alert){
					alert.style.display = "none";
				}
				
				formImputs[0].value = "";
				formImputs[1].value = "";

				modal.style.display = "none";
				return false;
			}
		}
		
		// create the callbaks function
		$scope.modalEvent();

		/* this function sent a requets to the serve to save the new big account*/
		$scope.submit = function() {
			//create new form
			var formData = new FormData;

			// it appends all the inputs data to the formData
			for(key in $scope.account) {
				formData.append(key, $scope.account[key]);
			}

			// get the input file and appends to the formData
			var file = document.getElementById('logo').files[0];
			formData.append("logo", file);

			/* request to save the new account */
			$http.post('/bigAccount/add',formData,{
				transformRequest: angular.identity,
				headers:{
					'Content-Type': undefined
				}

			}).then(function(res){
				if(res.data.auth){
					window.location = "/admin/login";
				}else{
				
					if(res.data.error){

						var divError = document.createElement("DIV");
						divError.classList.add("alert-danger");
						divError.innerHTML= res.data.error;

						var formAddBigAccount = modal.getElementsByClassName('form-add-bigAccount')[0];
						var form = modal.getElementsByTagName('form')[0];
						var h2 = modal.getElementsByTagName('h2')[0];
						form.parentNode.insertBefore(divError, h2.nextSibling);

					}else{
						// save the information result
						var account = res.data[0];

						var bigAccounts = document.getElementsByClassName('big-counts')[0];

						// create the structure to save the new account created
						var account = createNewAccount(account);

						// add the new structure to the bigAccounts list
						bigAccounts.appendChild(account);
						
						//clean the form
						var formImputs = modal.getElementsByClassName("inputsForm");
						var alert = modal.getElementsByClassName('alert-danger')[0];
						
						if(alert){
							alert.style.display = "none";
						}
						
						formImputs[0].value = "";
						formImputs[1].value = "";

						// close the modal
						modal.style.display = "none";
					}
				}
			});
		}

		/*
		* this function create a new structure account and return it
		*/
		function createNewAccount(account){
			var divBigAccount = document.createElement("DIV");
			var logoBigCount = document.createElement("DIV");
			var logoName = document.createElement("DIV");
			var href = document.createElement("A");
			var img = document.createElement("IMG");
			var h3 = document.createElement("H3");

			divBigAccount.classList.add("big-count");
			logoBigCount.classList.add("logo-big-count");
			logoName.classList.add("logo-name");

			img.src = account.logo;
			href.href = '/brands/' + account.id_big_accounts;
			href.appendChild(img);
			logoBigCount.appendChild(href);

			h3.innerHTML= account.name;
			logoName.appendChild(h3);

			divBigAccount.appendChild(logoBigCount);
			divBigAccount.appendChild(logoName);

			return divBigAccount
		}
	}


})();