(function() {
	'use strict';
	
	angular
		.module('myapp')
		.controller('assignNewBrandController',assignNewBrands);

	assignNewBrands.$inject = [
		'$scope',
		'$http'
	];

	function assignNewBrands($scope,$http){
		var mainDiv = document.getElementsByClassName('select-container')[0];
		var brandsAssigned = document.getElementsByClassName('assign-new-brand-container')[0];

		if(mainDiv){
			var btnsContainer = mainDiv.getElementsByClassName("btns-container")[0];
			var assignUser = btnsContainer.getElementsByClassName("assign-user")[0];
		}

		$scope.showsCount = function(account){

			$http({
				method: 'POST',
				url: '/assignBrand/getBrands',
				data:{'id_account':account}
			}).then(function successCallback(response) {

				$scope.brands = response.data.brands;

				if($scope.brands.length > 0){

					
					var divteams = mainDiv.getElementsByClassName('div-teams')[0];
					divteams.classList.remove("width-zero");
					var boderLightColor = divteams.getElementsByTagName('div')[0];	    
					var selectTeam = divteams.getElementsByClassName('select-teams')[0];

					divteams.style.marginBottom = "50px";

					boderLightColor.style.opacity = 1;	  

					if(typeof(selectTeam) !== 'undefined'){
						selectTeam.classList.remove("select-teams");
					}

					boderLightColor.classList.add("border-light-color");
					boderLightColor.classList.add("showTeamsDropdown");
				}
			}, function errorCallback(response) {

			});
		}

		$scope.changeTeam = function(selected){
			btnsContainer.style.display = "block";
		}

		$scope.changeUserBrands = function(){

			var selects = mainDiv.getElementsByTagName('select');
			var bigAccount = selects[0].value;
			var brand = selects[1].value;

			var url = window.location.pathname;
			var id = url.substring(url.lastIndexOf('/') + 1);
			
			var data = {
				'id_big_accounts' : bigAccount,
				'brands_id_brands' : brand,
				'id_users' : id
			};

			$http({
				method: 'POST',
				url: '/assignBrand/newBrand',
				data:data
			}).then(function successCallback(response) {
				var deleteMemberModal = brandsAssigned.getElementsByClassName('deleteMemberModal')[0];
				deleteMemberModal.style.display = "none";
				var message = document.getElementsByClassName("message")[0];

				if(response.data.error){

					message.innerHTML = response.data.error;
					message.classList.add("warning");

				}else{

					message.innerHTML = response.data.msg;
					message.classList.add("success");

				}

				message.style.display = "block"

			}, function errorCallback(response) {

			});
		}

		$scope.btnDelete = function(){

			var deleteMemberModal = brandsAssigned.getElementsByClassName('deleteMemberModal')[0];
			deleteMemberModal.style.display = "block";
		}

		$scope.btnCancel = function(){

			var deleteMemberModal = brandsAssigned.getElementsByClassName('deleteMemberModal')[0];
			deleteMemberModal.style.display = "none";
		}

		
		if(mainDiv){

			assignUser.addEventListener('click',function(){
				
				var deleteMemberModal = brandsAssigned.getElementsByClassName('deleteMemberModal')[0];
				deleteMemberModal.style.display = "block";

			});
		}
	}

})();