angular.module('myapp').controller('teamController',team);

team.$inject = [
	'$scope',
	'$http'
];

function team($scope,$http){
	
	var vm = this;
	$scope.user={};
	var team = document.getElementsByClassName('team')[0];
	modal = team.getElementsByClassName('modal')[0];
	var cancel = modal.getElementsByClassName("cancel")[0];

	/* This function enables the bottons to edit and delete a member */
	$scope.enable = function(id){
		
		var tagTr = document.getElementById(id);
		var checkBox = tagTr.getElementsByTagName("input")[0];
		var buttons = tagTr.querySelectorAll("button");
		var isChecked = checkBox.checked;

		/* it checks if the checkbox is selected or not */
		if(isChecked){
			isChecked = false;
		}else{
			isChecked = true;
		}

		for (var i = 0; i < buttons.length; i++) {
			/* this is to be enable or disable the buttons*/
			if(i <= 1){
				buttons[i].disabled = isChecked;
			}
			/* this is to be enable or disable the inputs*/
			buttons[i].disabled = isChecked;
		}
	}
	
	/* This function display the modal to add a new member */
	$scope.showModalAddMember = function(){
		modal.style.display = "block";
	}

	/* This function cancels the modal and clean all the inputs fields */
	cancel.onclick = function() {
		var formImputs = modal.getElementsByClassName("inputsForm");
		var alert = modal.getElementsByClassName('alert-danger')[0];
		
		if(alert){
			alert.style.display = "none";
		}
		
		for (var i = 0; i < formImputs.length; i++) {
			formImputs[i].value = "";
		}

		modal.style.display = "none";
		return false;
	}


	$scope.submit = function() {
		//create new form
		var formData2 = new FormData;
		// it appends all the inputs data to the formData
		for(key in $scope.user){
			formData2.append(key, $scope.user[key]);
		}

		console.log(formData2)
		console.log($scope.user);

		// // get the input file and appends to the formData
		// var file = document.getElementById('avatar').files[0];
		// var id = document.getElementById('id');

		// formData.append("brand", id.value);
		// formData.append("avatar", file);

		// console.log($scope.user);
		// var test={"data":"test"};

		/* request to save the new team */
		$http.post('/team/add',formData2,{
			transformRequest: angular.identity,
			headers:{
				'Content-Type': undefined
			}
		}).then(function(res){
			console.log(res.data);
			if(res.data.auth){
				window.location = "/admin/login";
			}
			// if(res.data.auth){
			// 	window.location = "/admin/login";
			// }else{
			
			// 	if(res.data.error){

			// 		var divError = document.createElement("DIV");
			// 		divError.classList.add("alert-danger");
			// 		divError.innerHTML= res.data.error;

			// 		var form = modal.getElementsByTagName('form')[0];
			// 		var h2 = modal.getElementsByTagName('h2')[0];
			// 		form.parentNode.insertBefore(divError, h2.nextSibling);

			// 	}else{
			// 		// save the information result
			// 		var brand = res.data[0];

			// 		var brands = document.getElementsByClassName('brands')[0];

			// 		// create the structure to save the new account created
			// 		var newBrand = createNewBrand(brand)

			// 		// add the new structure to the bigAccounts list
			// 		brands.appendChild(newBrand);
					
			// 		//clean the form
			// 		var formImputs = modal.getElementsByClassName("inputsForm");
			// 		var alert = modal.getElementsByClassName('alert-danger')[0];
					
			// 		if(alert){
			// 			alert.style.display = "none";
			// 		}
					
			// 		formImputs[0].value = "";
			// 		formImputs[1].value = "";

			// 		// close the modal
			// 		modal.style.display = "none";
			// 	}
			// }
		});		

	}
	// $scope.submit();



}


































