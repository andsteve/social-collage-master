angular.module('myapp').controller('brandsController',brands);

brands.$inject = [
	'$scope',
	'$http'
];

function brands($scope,$http){
	
	var brandsContainer = document.getElementsByClassName('brandsContainer')[0];
	var modal = document.getElementById('myModal');
	var cancel = modal.getElementsByClassName("cancel")[0];
	
	/* display the model to add a new brand*/
	$scope.modalEvent = function(id_big_account){		
		modal.style.display = "block";
	}

	cancel.onclick = function() {
		var formImputs = modal.getElementsByClassName("inputsForm");
		var alert = modal.getElementsByClassName('alert-danger')[0];
		
		if(alert){
			alert.style.display = "none";
		}
		
		formImputs[0].value = "";
		formImputs[1].value = "";

		modal.style.display = "none";
		return false;
	}

	/* this function sent a requets to the serve to save the new big account*/
	$scope.submit = function() {
		//create new form
		var formData = new FormData;

		// it appends all the inputs data to the formData
		for(key in $scope.brand){
			formData.append(key, $scope.brand[key]);
		}

		// get the input file and appends to the formData
		var file = document.getElementById('logo').files[0];
		var id = document.getElementById('id');

		formData.append("id", id.value);
		formData.append("logo", file);

		/* request to save the new brand */
		$http.post('/brand/add',formData,{
			transformRequest: angular.identity,
			headers:{
				'Content-Type': undefined
			}
		}).then(function(res){
			
			if(res.data.auth){
				window.location = "/admin/login";
			}else{
			
				if(res.data.error){

					var divError = document.createElement("DIV");
					divError.classList.add("alert-danger");
					divError.innerHTML= res.data.error;

					var form = modal.getElementsByTagName('form')[0];
					var h2 = modal.getElementsByTagName('h2')[0];
					form.parentNode.insertBefore(divError, h2.nextSibling);

				}else{
					// save the information result
					var brand = res.data[0];

					var brands = document.getElementsByClassName('brands')[0];

					// create the structure to save the new account created
					var newBrand = createNewBrand(brand)

					// add the new structure to the bigAccounts list
					brands.appendChild(newBrand);
					
					//clean the form
					var formImputs = modal.getElementsByClassName("inputsForm");
					var alert = modal.getElementsByClassName('alert-danger')[0];
					
					if(alert){
						alert.style.display = "none";
					}
					
					formImputs[0].value = "";
					formImputs[1].value = "";

					// close the modal
					modal.style.display = "none";
				}
			}
		});
	}

	/*
	* this function create a new structure brand and return it
	*/
	function createNewBrand(brand){

		var divBrand = document.createElement("DIV");
		var logoBrand = document.createElement("DIV");
		var href = document.createElement("A");
		var img = document.createElement("IMG");
		var h2 = document.createElement("H2");

		divBrand.classList.add("brand");
		logoBrand.classList.add("logo-brand");

		img.src = brand.logo;
		href.href = '/team/' + brand.id_brands;
		href.appendChild(img);
		logoBrand.appendChild(href);

		h2.innerHTML= brand.name;

		divBrand.appendChild(logoBrand);
		divBrand.appendChild(h2);

		return divBrand;
	}


	
}


































