//EXPRESS
var express = require('express');
var multer  = require('multer');
var router = express.Router();

//MODELS
var bigAccountsModel = require('../models/bigAccountsModel');

//PATHCONFIG
var path_config = require('./path_config');

//ROUTES

/* Route to add new big account */
router.post('/bigAccount/add', addBigAccount);

/* Route to update the status of the big account */
router.post('/bigAccount/updateStatus',updateBigAccountStatus);

/* This function move the image seleted to another path*/
var storage = multer.diskStorage({

	/* destination where the image are going be saved */
	destination: function (request, file, callback) {

		callback(null, path_config.imagesFolders.big_counts);
	},
	filename: function (request, file, callback) {
		var valid = false;

		/* this function checks if the file selected is valid or not */
		valid = isValidImage(file.mimetype);

		/* check if the image was valid and if the number of the user is not empty */
		if(valid && request.body.name){

			var originalnameSplitted = file.mimetype.split('/');

			/* update the image name with the user details */
			file.originalname = request.body.name+"."+originalnameSplitted[1];

			/* move the image selected to the destination path */
			callback(null, file.originalname);

		}else{

			/* send error */
			return callback(null, '', new Error('I don\'t have a clue!'));
		}
	}
});

/* this function check if the file is a allow image */
function isValidImage(mimetype){
	var listValidImages = ['image/jpeg','image/png','image/jpg'];

	for(var i = 0; i < listValidImages.length; i++ ){
		
		if(listValidImages[i] == mimetype){
			
			return true;
		}

	}

	return false;
}

/* moves the image selected to another path */
var upload = multer({ storage: storage }).single("logo");

function addBigAccount(req, res, next) {

	/* checks if there is someone authenticated */
	if(req.isAuthenticated()){

		/* move the image to the destination path */
		upload(req, res, function (err) {

			/* checks if the there was any error moving the image or if the image is not valid */
			if (err) {

				var error = {
					'error' : "The file must be an image file"
				}

				res.send(error);

			}else{

				/* split the image by "build/" */
				var path = req.file.path.split('build/');

				accountsData={
					'name' : req.body.name,
					'active' : false,
					'logo' : '/'+path[1]
				}

				/* insert big account */
				bigAccountsModel.insertAccount(accountsData,function(error, accounts){

					/* gets big account inserted */
					bigAccountsModel.getBigAccount(accounts.insertId,function(error, accounts){
						res.send(accounts);
					});
				});
			}
		});

	}else{

		var error = {
			'auth' : "is not authenticated"
		};

		res.send(error);
	}
}

/* this funtion updates the status of the big account */
function updateBigAccountStatus (req,res){

	/* checks if there is someone authenticated */
	if(req.isAuthenticated()){

		var big_account = req.body;

		/* update the status of the big account */
		bigAccountsModel.updateStatus(big_account,function(err,message){
			res.send(message);
		});

	}else{

		var error = {
			'auth' : "is not authenticated"
		};

		res.send(error);
	}
}

module.exports = router;
