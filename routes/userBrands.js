//EXPRESS
var express = require('express');
var router = express.Router();

//ENCRIPT
var bcrypt = require('bcrypt-nodejs');

//MODELS
var userBrandsModel = require('../models/userBrandsModel');
var bigAccountsModel = require('../models/bigAccountsModel');
var brandsModel = require('../models/brandsModel');
var rolesModel = require('../models/rolesModel');
var usersModel = require('../models/usersModel');

//AUTHTENTICATE
var authenticated = require('./authenticated');

//ROUTES

/* Route to update the role of the user */
router.post('/userbrands/updateRole', updateRole);

/* Route to show the view "assign new brand" */
router.get('/assignBrand/:id', authenticated.ensureAuthenticated, assignBrand);

/* Route to get all brands of the one accounts */
router.post('/assignBrand/getBrands', getBrands);

/* Route to assing an user to another brand */
router.post('/assignBrand/newBrand', authenticated.ensureAuthenticated, newBrand);

/* this function update the role of the user */
function updateRole(req,res){

	/* checks if there is someone authenticated */
	if(req.isAuthenticated()){

		var userBrands = req.body;

		/* update the role of the user */
		userBrandsModel.updateRole(userBrands,function(err,message){

			/* update password for a user */
			usersModel.getUserById(userBrands.id_user,function(err,user){

				/* checks if the user already has a password */
				if(user[0].password === ""){

					/* encript default password */
					bcrypt.hash("12345", null, null, function(err, hash) {

						var userData = {
							'password':hash,
							'id_users':userBrands.id_user
						};

						/* update password for a user */
						usersModel.updatePassword(userData,function(err,user_brand){

						});
					});
				}
			});

			res.send(message);
		});

	}else{

		var error = {
			'auth' : "is not authenticated"
		}

		res.send(error);
	}
}

/* this function render to the view assign new brand */
function assignBrand (req,res){

	/* get all big accounts */
	bigAccountsModel.getAccounts(function(error, accounts){

		/* checks if the user selected if different to the user logged */
		if(req.params.id != req.user.id){

			/* gets all users brands of the users selected */
			userBrandsModel.getBrandJoinedBrandUser(req.params.id,function(err,user_brand){

				/* checks if the user logged is a SuperAdmin */
				if(req.user.role === 'SuperAdmin'){

					var data = {
						'user' : {
							'id': req.params.id
						},
						'bigAccounts': accounts,
						'userBrand': user_brand
					};

					/* show the view assignNewBrand */
					res.render('assignNewBrand',data);

				}else{

					/* gets all users brands of the users logged */
					userBrandsModel.getBrandUser(req.user.id,function(err,userBrandsLogged){

						var resFind2 = false;

						/* checks if the user selected exists in the user brands of the user logged */
						var resFind = user_brand.find(function(userBrand){

							for (var i = 0; i < userBrandsLogged.length; i++) {
								
								if(userBrand.brands_id_brands === userBrandsLogged[i].brands_id_brands){
									resFind2 = true;
									break;
								}                       
							}
							
							return resFind2;
						});

						/* checks if the user selected can be reassigned by the user logged */
						if(resFind){

							/* find the big account of the user logged */
							var bigAccountsccount = accounts.find(function(bigAccounts){
								return (bigAccounts.id_big_accounts === resFind.id_big_accounts);
							});

							var account = [bigAccountsccount];

							var data = {
								'user' : {
									'id': req.params.id
								},
								'bigAccounts': account,
								'userBrand': user_brand
							};

							/* show the view assignNewBrand */
							res.render('assignNewBrand',data);
						
						}else{
							
							var data = {
								'error' : "You don't have permition to reassign the user selected to another big account"
							};
							
							/* show the view assignNewBrand */
							res.render('assignNewBrand',data);
						}
					});
				}
			});

		}else{
			
			var data = {
				'error' : "You don't have permition to reassign yourself in another big account"
			};
			
			/* show the view assignNewBrand */
			res.render('assignNewBrand',data);
		}
	});
}

/* this function gets all brands of the one accounts */
function getBrands (req,res){

	/* get all brands of the big accounts */
	brandsModel.getAccountsOfBigAccount(req.body.id_account,function(error, brands){

		/* send data */
		res.send({"brands":brands});
	});
}

/* this function assigns an user to another brand */
function newBrand (req,res){

	var data = req.body;

	/* gets all users brands where user selected is assigned */
	userBrandsModel.getBrandUser(data.id_users,function(err,user_brand){

		/* find if the user selected already exist in the brand selected */
		var resFind = user_brand.find(function(userBrand){
			return ((userBrand.brands_id_brands === parseInt(data.brands_id_brands)) && 
					(userBrand.users_id_users === parseInt(data.id_users)));
		});

		/* if the user doesn't exist */
		if(!resFind){

			/* get role of the user selected */
			rolesModel.getRoleUser(user_brand[0].roles_id_role,function(err,roles){

				/* checks if the role of the user is a Super admin */
				if(roles[0].name == 'SuperAdmin'){

					res.send({"error":"You can't assign this user for any brand"});

				}else{

					/* checks if the role of the user selected is an admin */
					if(roles[0].name == 'Admin'){

						/* gets the brand does match with id brand of the users brands */
						brandsModel.getBrand(user_brand[0].brands_id_brands,function(error, brands){

							/* deletes all users brands of the user selected */
							userBrandsModel.deleteUserBrands(data.id_users,function(err,message){

								var object ={
									'brands_id_brands':data.brands_id_brands,
									'users_id_users':data.id_users,
									'roles_id_role': '2'
								};

								/* insert the new user brand */
								userBrandsModel.insertUserBrand(object,function(error, usersBrand){

									var userdata = {
										'id_users' :data.id_users, 
										'active' : false
									}

									/* update the status of the user selected to false */
									usersModel.updateStatus(userdata,function(err,message){

									});

									res.send({'msg': 'User was reassigned successfully!'});
								});
							});
						});

					}else{

						/* if the user is a simple user update to user brand */ 
						userBrandsModel.updateUserBrand(data,function(err,message){

							var userdata = {
								'id_users' :data.id_users, 
								'active' : false
							}

							/* update the status of the user selected to false */
							usersModel.updateStatus(userdata,function(err,message){

							});

							res.send(message);
						
						});
					}
				}
			});
		
		}else{
			res.send({'error': 'This user already exist in this brand!'});
		}
	});
}

module.exports = router;
