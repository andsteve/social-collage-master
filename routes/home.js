//EXPRESS
var express = require('express');
var router = express.Router();

//MODELS
var userNetworksModel = require('../models/userNetworksModel');
var userBrandsModel = require('../models/userBrandsModel');
var bigAccountsModel = require('../models/bigAccountsModel');
var brandsModel = require('../models/brandsModel');
var usersModel = require('../models/usersModel');

//ROUTES

/* home route */
router.get('/' , home );

/* route to get brands of the big account */
router.post('/home/getBrands' , getBrands );

/* route to find all users of the one brand */
router.get('/home/team/:id' , team );

/* this function gets all big accounts that are actives and renter to home view */
function home(req,res){

	/* gets all big accounts that are actives */
	bigAccountsModel.getAccountsActive(function(error, accounts){

		/* render to home view */
		res.render('home',{"accounts":accounts});
	});
}

/* this function get all brands actives of the big accounts and send data */
function getBrands(req,res){

	/* get all brnads actives of the big accounts */
	brandsModel.getAccountsActivesOfBigAccount(req.body.id_account,function(error, brands){

		/* send back data */
		res.send({"brands":brands});
	});
}

/* this function gets all users actives of the specific brand */
function team(req,res){
	var id_brand = req.params.id;

	/* gets all users ids */
	userBrandsModel.getUserBrandsById(id_brand,function(error, usersBrands){

		var usersIds=[];

		/* this for push all users id in array */
		for (var i = 0, len = usersBrands.length; i < len; i++) {
			usersIds.push(usersBrands[i].users_id_users);
		}

		/* checks if the are more than one users in the array */
		if(usersIds.length > 0){

			/* gets all users actives that does match with the usersIds array */
			usersModel.getUserActivesByIds(usersIds,function(err,users){

				/* get all social networks of each users */
				userNetworksModel.getUserNetworksByIds(usersIds,function(err,socialNetworks){

					/* gets new list of users with theirs social networks */
					var newusers = AddSocialNetworks(socialNetworks,users);

					/* add the id brand */
					newusers.id_brand = id_brand;

					/* send the list to the view */
					res.send({'users':newusers});
				});
			});
		}
	});
}
/* this function add all social networks for each user */
function AddSocialNetworks(socialNetworks,users){

	var social_networks=[];
	var number = 0;
	var sameUser = false;

	/* for runs all users */
	for (var i = 0; i < users.length; i++) {

		/* for runs all social networks */
		for (var c = 0; c < socialNetworks.length; c++) {

			if(users[i].id_users == socialNetworks[c].id_users){

				if(socialNetworks[c].name == 'facebook'){
					users[i].facebook = socialNetworks[c].user_name;
				}

				if(socialNetworks[c].name=='twitter'){
					users[i].twitter = socialNetworks[c].user_name;
				}

				if(socialNetworks[c].name=='linkedin'){
					users[i].linkedin = socialNetworks[c].user_name;
				}

				if(socialNetworks[c].name=='instagram'){
					users[i].instagram = socialNetworks[c].user_name;
				}

				if(socialNetworks[c].name=='skype'){
					users[i].skype = socialNetworks[c].user_name;
				}
			}
		}
	}
	return users;
}
module.exports = router;
