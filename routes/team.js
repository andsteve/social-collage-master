//EXPRESS
var express = require('express');
var multer  = require('multer');
var router = express.Router();

//MODELS
var socialNetworksModel = require('../models/socialNetworksModel');
var userNetworksModel = require('../models/userNetworksModel');
var userBrandsModel = require('../models/userBrandsModel');
// var bigAccounts = require('../models/bigAccountsModel');
var usersModel = require('../models/usersModel');

//PATHCONFIG
var path_config = require('./path_config');

//ROUTES

/* Route to add new user for a team */
router.post('/team/add', addUserToTeam);

/* Route to delete an user */
router.post('/user/deleteUser', deleteUser);

/* This function move the image seleted to another path*/
var storage = multer.diskStorage({

	/* destination where the image are going be saved */
	destination: function (request, file, callback) {
		/* gets the user by his number */
		usersModel.getUser(request.body.number, function(err, user) {

			/* checks if the user is not register */
			if(user.length == 0){

				callback(null, path_config.imagesFolders.team);
			}else{

				/* send error */
				callback('UserError',new Error())
			}
		});
	},

	filename: function (request, file, callback) {
		var valid = false;

		/* gets true if the image is valid or false is not */
		valid = isValidImage(file.mimetype);

		/* checks if the image is valid and if the field name is not empty */
		if(valid && request.body.number){

			var originalNameSplitted = file.mimetype.split('/');

			/* update the image name with the user details */
			file.originalname = request.body.number+"."+originalNameSplitted[1];

			/* move the image selected to the destination path */
			callback(null, file.originalname);

		}else{

			/* send error */
			callback('FileError',new Error())
		}
	}
});

/* this function check if the file is a allow image */
function isValidImage(mimetype){
	var listValidImages = ['image/jpeg','image/png','image/jpg'];

	for(var i = 0; i < listValidImages.length; i++ ){
		
		if(listValidImages[i] == mimetype){
			
			return true;
		}
	}

	return false;
}

/* this function add one by one social netwok of the user */
function addSocialNeworks(socialNetworks,UserId){

	/* gets all social networks */
	socialNetworksModel.getsocialNetwoks(function(err,allSocialNetwoks){
		var redSocial  = {};
		var isUndefined = true;

		/* runs social networks of the user inserted */
		for (var i = 0; i < socialNetworks.length; i++) {

			if (socialNetworks[i].facebook !== undefined) {
				isUndefined = false;
				redSocial.name = 'facebook';
				redSocial.userName = socialNetworks[i].facebook;
			}
			if (socialNetworks[i].twitter !== undefined) {
				isUndefined = false;
				redSocial.name = 'twitter';
				redSocial.userName = socialNetworks[i].twitter;
			}
			if (socialNetworks[i].linkedin !== undefined) {
				isUndefined = false;
				redSocial.name = 'linkedin';
				redSocial.userName = socialNetworks[i].linkedin;
			}
			if (socialNetworks[i].instagram !== undefined) {
				isUndefined = false;
				redSocial.name = 'instagram';
				redSocial.userName = socialNetworks[i].instagram;
			}
			if (socialNetworks[i].skype !== undefined) {
				isUndefined = false;
				redSocial.name = 'skype';
				redSocial.userName = socialNetworks[i].skype;
			}

			/* checks if the social networks is found */
			if (!isUndefined) {

				/* ones the social networks id found finds which does match with the database */
				for (var c = 0; c < allSocialNetwoks.length; c++) {

					if(allSocialNetwoks[c].name == redSocial.name ){

						var UserNetworkData ={
						'social_networks_id_social_networks' : allSocialNetwoks[c].id_social_networks,
						'id_users' : UserId,
						'user_name' : redSocial.userName 
						}

						/* insert new social network for an user inserted */
						userNetworksModel.insertUserNetworks(UserNetworkData,function(err,mySocialNetworks){

						});
					}
				}

				/* set default values */
				isUndefined = true;
				redSocial.name = '';
				redSocial.userName = '';
			}
		}
	});
}

/* moves the image selected to another path */
var upload = multer({ storage: storage }).single("avatar");

/* this function add new user for a team */
function addUserToTeam(req, res, next){

	/* checks if there is someone authenticated */
	if(req.isAuthenticated()){

		/* move the image to the destination path */
		upload(req, res, function (err) {

			/* this is to checks if the image file was selected */
			if(req.file){

				/* split the image by "build/" */
				var path = req.file.path.split('build/');

				req.body.avatar = '/' + path[1];

			}else{

				/* add default image for the new user fo the team  */
				req.file = path_config.imagesFolders.defaultUser;
				req.body.avatar = req.file
			}

			/* checks if the there was any error moving the image or if the image is not valid */
			if (err) {
				var error ={}

				if(err == "UserError"){

					error.userError = "the user already exit";

				}else{

					error.fileError = "The file must be an image file";
				}

				res.send(error);

			}else{

				/* userData to insert */
				var userData = {
					'name' : req.body.name,
					'lastname' : req.body.lastName,
					'number' : req.body.number,
					'avatar' : req.body.avatar,
					'active' : false,
					'password' : ''
				}

				/* all all social network in array */
				var socialNetworks = [];
				socialNetworks.push({"facebook":req.body.facebook});
				socialNetworks.push({"twitter":req.body.twitter});
				socialNetworks.push({"linkedin":req.body.linkedin});
				socialNetworks.push({"instagram":req.body.instagram});
				socialNetworks.push({"skype":req.body.skype});

				/* insert the new user */
				usersModel.insertUser(userData, function(err, user) {

					var object ={
						'brands_id_brands':req.body.brand,
						'users_id_users':user.insertId,
						'roles_id_role': '3'
					};

					/* add his or her social networks */
					addSocialNeworks(socialNetworks,user.insertId);

					/* add the user inserted to the brand */
					userBrandsModel.insertUserBrand(object,function(error, usersBrand){

						req.body.id_users = user.insertId

						res.send(req.body);
					});
				});
			}
		});
	}else{

		var error = {
			'auth' : "is not authenticated"
		};

		res.send(error);
	}
}

/* this funcion delete a user from a team */
function deleteUser(req, res, next) {

	/* checks if there is someone authenticated */
	if(req.isAuthenticated()){

		var userBrandsDeleted = false;
		var usersNetworksDeleted = false;

		/* delete users brands of the user that are going to be deleted */
		userBrandsModel.deleteUserBrands(req.body.id,function(error, deleted){

			userBrandsDeleted = deleted.msg

			/* delete all social network from the user that are going to be deleted */
			userNetworksModel.deleteUserNetworskByIdUser(req.body.id,function(error, deleted){

				usersNetworksDeleted = deleted.msg

				/* check if user brands and socail networks were deleted */
				if(userBrandsDeleted && usersNetworksDeleted){

					/* delete the user form a team */
					usersModel.deleteUser(req.body.id,function(error, usersDeleted){

						res.send(usersDeleted);
					});
				}
			});
		});

	}else{

		var error = {
			'auth' : "is not authenticated"
		};

		res.send(error);
	}
}

module.exports = router;