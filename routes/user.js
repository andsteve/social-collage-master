//EXPRESS
var express = require('express');
var multer  = require('multer');
var router = express.Router();

//MODELS
var socialNetworksModel = require('../models/socialNetworksModel');
var userNetworksModel = require('../models/userNetworksModel');
var userBrandsModel = require('../models/userBrandsModel');
var rolesModel = require('../models/rolesModel');
var usersModel = require('../models/usersModel');

//AUTHTENTICATE
var authenticated = require('./authenticated');

//PATHCONFIG
var path_config = require('./path_config');

//DELETE FILES
const del = require('del');

//ROUTES

/* Route to render the profile of the specific user */
router.get('/user/:id', authenticated.ensureAuthenticated, showProfile);

/* Route to update the personal information of the user */
router.post('/user/update', updatePersonalInfo);

/* Route to update the user name of the social network */
router.post('/userNetwork/update', updateSocialNetwork);

/* Route to delete a social network of the user */
router.post('/userNetwork/delete', deleteSocialNetwork);

/* Route to update the avatar of the user */
router.post('/user/updateAvatar', updateAvatar);

/* Route to update the status of the user */
router.post('/user/updateStatus', updateStatus);

/* This function move the image seleted to another path */
var storage = multer.diskStorage({

	/* destination where the image are going be saved */
	destination: function (request, file, callback) {

		callback(null, path_config.imagesFolders.team);
	},

	filename: function (request, file, callback) {
		var valid = false;

		/* this function checks if the file selected is valid or not */
		valid = isValidImage(file.mimetype);

		/* check if the image was valid and if the number of the user is not empty */
		if(valid && request.body.number){

			/* get the full current date */
			var datetime = getFullDate();

			/* get the mimetype of the image selected */
			var mimetypeReverse = getMimetype(file.originalname);

			/* save the mimetype to the request body */
			request.body.newMimeType = mimetypeReverse;

			/* update the image name with the user details */
			file.originalname = request.body.number+"-"+datetime+"."+mimetypeReverse;

			/* move the image selected to the destination path */
			callback(null, file.originalname);

		}else{

			/* show and error if the file is not a image or the user has an empty number */
			callback('FileError',new Error())
		}
	}
});

/* gets full current date */
function getFullDate(){

	date = new Date();
	var year = date.getFullYear();
	var month = date.getMonth();
	var day = date.getDay();

	var hour = date.getHours();
	var minutes = date.getMinutes();
	var seconds = date.getSeconds();

	var fullDate = year+":"+month+":"+day+":"+hour+":"+minutes+":"+seconds;
	return fullDate;
}

/*
* this function check if the file is a allow image
*/
function isValidImage(mimetype){
	var listValidImages = ['image/jpeg','image/png','image/jpg'];

	for(var i = 0; i < listValidImages.length; i++ ){
		
		if(listValidImages[i] == mimetype){
			return true;
		}
	}

	return false;
}

/*
* this function get the mimetype
*/
function getMimetype(originalname){
	var mimetype = '';

	for (var i = originalname.length-1; i >= 0; i--) {

		if(originalname[i] == '.'){
			break;
		}

		mimetype += originalname[i];
	}

	var mimetypeReverse = mimetype.split("").reverse().join('');

	return mimetypeReverse;
}

/* moves the image selected to another path */
var upload = multer({ storage: storage }).single("avatar");

/* function to render to the user profile */
function showProfile (req,res){

	var user_id = [req.params.id];

	/* get user selected */
	usersModel.getUserById(user_id, function(err,users){

		if(users.length > 0){

			var userId = [users.id_users];

			/* gets the user brand to know witch is the role of that user */
			userBrandsModel.getBrandUser(user_id,function(err,user_brand){

				/* gets all user brands of the user that is logged */
				userBrandsModel.getBrandUser(req.user.id,function(error, userBrandsLogged){

					var resFind2 = false;

					/* checks if the user logged is the maneger of the user selected */
					var resFind = user_brand.find(function(userBrand){

						for (var i = 0; i < userBrandsLogged.length; i++) {

							if(userBrand.brands_id_brands === userBrandsLogged[i].brands_id_brands){
								resFind2 = true;
								break;
							}                       
						}

						return resFind2;
					});            

					/* checks if the user logged is the maneger of the user selected or if the user logged is and SuperAdmin*/
					if(resFind || req.user.role === 'SuperAdmin'){

						/* gets social networks of the use selected */
						userNetworksModel.getUserNetworksByIds(user_id,function(err,socialNetworks){

							/* gets all roles of the database */
							rolesModel.getRoles(function(err,roles){    

								/* checks if the user who is authenticated is not who was selected */                            
								if(req.user.id !== parseInt(req.params.id)){

									/* finds the role of the user selected */
									for (var i = 0; i < roles.length; i++) {

										if(roles[i].id_role === user_brand[0].roles_id_role ){
											
											roles[i].user = true;
											break;
										}
									}

									/* if the person is not a SuperAdmin remove the SuperAdmin role */
									if(req.user.role != 'SuperAdmin'){

										for (var i = 0; i < roles.length; i++) {

											if(roles[i].name == 'SuperAdmin'){
												
												roles.splice(i, 1); 
											}
										}
									}

								}else{

									/* if the user selected is the same user who is logged */
									roles.length = 0;

									roles.push(
										{
											'user':true,
											'name':req.user.role,
											'id_role':req.user.id_role
										}
									);
								}

								var user = {
									'persona_info':users,
									'socialNetworks':socialNetworks,
									'roles' : roles
								};

								/* render to the profile of the user selected */
								res.render('profileMember',user);
							});
						});
					}else{
						/* redirect to admin */
						res.redirect("/admin");
					}
				});
			});
		}else{

			/* redirect to admin */
			res.redirect("/admin");
		}
	});
}

/* function to update the personal information of the user */
function updatePersonalInfo (req,res){

	/* checks if there is someone authenticated */
	if(req.isAuthenticated()){

		var user = req.body;

		/* update personal information */
		usersModel.updateUser(user,function(err,message){

			/* get user updated */
			usersModel.getUserById(user.id_users,function(err,user){

				var userAndMessage = {
					"user":user,
					"message":message
				};

				/* send user updated */
				res.send(userAndMessage);
			});
		});
	}else{

		var error = {
			'auth' : "is not authenticated"
		};

		res.send(error);
	}

}

/* function to update username of the specific social network */
function updateSocialNetwork (req,res){

	/* checks if there is someone authenticated */
	if(req.isAuthenticated()){
		var userNetwork = req.body;

		/* update username of the specific social network */
		userNetworksModel.updateUserNetwork(userNetwork,function(err,message){

			/* gets user network updated */
			userNetworksModel.getUserNetwork(userNetwork,function(err,userNetworkUpdated){

				/* gets social network */
				socialNetworksModel.getsocialNetwokById(userNetwork.id_social,function(err,socialNetwork){

					userNetworkUpdated[0].link = socialNetwork[0].link;

					var userNetworkAndMessage = {
						"userNetwork":userNetworkUpdated,
						"message":message
					};

					/* send social network updated */
					res.send(userNetworkAndMessage);
				});
			});
		});

	}else{

		var error = {
		'auth' : "is not authenticated"
		};

		res.send(error);
	}
}

/* function to delete a social network of the user */
function deleteSocialNetwork (req,res){

	/* checks if there is someone authenticated */
	if(req.isAuthenticated()){

		var userNetwork = req.body;

		/* delete the social network */
		userNetworksModel.deleteUserNetwork(userNetwork,function(err,message){

			res.send(message);
		});

	}else{

		var error = {
			'auth' : "is not authenticated"
		};

		res.send(error);
	}
}

/* function to update the avatar of the user */
function updateAvatar (req,res){

	/* checks if there is someone authenticated */
	if(req.isAuthenticated()){

		/* move the image to the destination path */
		upload(req, res, function (err) {

			/* location of the old avatar */
			var location = "build" + req.body.exAvatar;

			/* remove the old avatar from the folder */
			del([location]);

			/* create the objete to update the avatar */
			var user = {
				"avatar": path_config.imagesFolders.userAvatar +"/"+ req.file.originalname,
				"id_user":req.body.id_user,
			};

			/* update the avatar */
			usersModel.updateUserAvatar(user,function(err,message){

				/* get the uset with the avatar updated */
				usersModel.getUserById(user.id_user,function(err,userUpdated){

					/* get the success message updated */
					userUpdated[0].message = message;

					/* send te user updated */
					res.send(userUpdated);
				});
			});
		});
	}else{

		var error = {
			'auth' : "is not authenticated"
		};

		res.send(error);
	}
}

/* function to update the status of the user */
function updateStatus(req,res){

	/* checks if there is someone authenticated */
	if(req.isAuthenticated()){

		var user = req.body;

		/* update the status of the user */
		usersModel.updateStatus(user,function(err,message){

			res.send(message);
		});

	}else{

		var error = {
			'auth' : "is not authenticated"
		};

		res.send(error);
	}
}

module.exports = router;