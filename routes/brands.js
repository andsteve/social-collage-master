//EXPRESS
var express = require('express');
var multer  = require('multer');
var router = express.Router();

//MODELS
var userNetworksModel = require('../models/userNetworksModel');
var userBrandsModel = require('../models/userBrandsModel');
var bigAccountsModel = require('../models/bigAccountsModel');
var brandsModel = require('../models/brandsModel');
var usersModel = require('../models/usersModel');

//AUTHTENTICATE
var authenticated = require('./authenticated');

//PATHCONFIG
var path_config = require('./path_config');

//ROUTES

/* Route to get all brands of the big accounts */
router.get('/brands/:id', authenticated.ensureAuthenticated, getBrands);

/* Route to add a new brand to the big account */
router.post('/brand/add', addNewBrand);

/* Route to update the status of the brand */
router.post('/brand/updateStatus', updateBrandStatus);

/* Route to get all users of the team */
router.get('/team/:id', authenticated.ensureAuthenticated, getTeam);

/* This function move the image seleted to another path*/
var storage = multer.diskStorage({

	/* destination where the image are going be saved */
	destination: function (request, file, callback) {
		
		callback(null, path_config.imagesFolders.brands);
	},

	filename: function (request, file, callback) {
		var valid = false;

		/* this function checks if the file selected is valid or not */
		valid = isValidImage(file.mimetype);

		/* it checks if the file is allowed and if the field name is not empty */
		if(valid && request.body.name){

			var originalnameSplitted = file.mimetype.split('/');

			/* update the image name with the user details */
			file.originalname = request.body.name+"."+originalnameSplitted[1];

			/* move the image selected to the destination path */
			callback(null, file.originalname);

		}else{

			/* send error */
			return callback(null, '', new Error('I don\'t have a clue!'));
		}
	}
});

/* this function check if the file is a allow image */
function isValidImage(mimetype){

	var listValidImages = ['image/jpeg','image/png','image/jpg'];

	for(var i = 0; i < listValidImages.length; i++ ){

		if(listValidImages[i] == mimetype){

			return true;
		}
	}

	return false;
}

/* moves the image selected to another path */
var upload = multer({ storage: storage }).single("logo");

function getBrands(req,res){

	var id_big_accounts = req.params.id;

	/* checks if the user logged is a SuperAdmin */
	if(req.user.role == 'SuperAdmin'){

		/* gets all brnads of the big account */
		brandsModel.getAccountsOfBigAccount(id_big_accounts,function(error, accounts){

			if(accounts.length > 0){
				/* gets a big account */
				bigAccountsModel.getBigAccount(id_big_accounts,function(error, bigAccount){

					var object ={
						'accounts':accounts,
						'bigAccount' : {
							'id_big_accounts':id_big_accounts,
							'name': bigAccount[0].name,
							'active': bigAccount[0].active
						}
					}

					/* render to brands view */
					res.render('brands',object);
				});
			}else{
				
				/* render to admin view */
				res.redirect('/admin');
			}
		});

	}else{
		/* gets all user brands of the user logged */
		userBrandsModel.getBrandUser(req.user.id,function(error, brand_user){

			var brandsIds=[];

			/* gets all id brands */
			for (var i = 0, len = brand_user.length; i < len; i++) {
				brandsIds.push(brand_user[i].brands_id_brands);
			}

			/* checks if the logged has at least one */
			if(brandsIds.length > 0){

				/* gets all brands by array of id brands */
				brandsModel.getAccountsByBrandIds(brandsIds,function(err,accounts){

					/* checks if the user logged tries to access another big account */
					if(accounts[0].id_big_accounts != id_big_accounts){

						/* redirect to admin if the user logged tries to access to another big account */
						res.redirect('/admin'); 

					}else{

					/* gets big account */
					bigAccountsModel.getBigAccount(id_big_accounts,function(error, bigAccount){

						var object ={
							'accounts':accounts,
							'bigAccount' : {
								'id_big_accounts':id_big_accounts,
								'name': bigAccount[0].name,
								'active': bigAccount[0].active
							}
						}

						/* render to brands view */
						res.render('brands',object);
					});
					}
				});
			}
		});
	}
}

/* this function add a new brand to the big account */
function addNewBrand(req, res, next) {

	/* checks if there is someone authenticated */
	if(req.isAuthenticated()){

		/* move the image to the destination path */
		upload(req, res, function (err) {

			/* checks if there are errors */
			if (err) {

				var error = {
					'error' : "The file must be an image file"
				};

				res.send(error);

			}else{

				/* split the image by "build/" */
				var path = req.file.path.split('build/');

				brandData={
					'name' : req.body.name,
					'logo' : '/'+path[1],
					'isEnable':false,
					'id_big_accounts':req.body.id
				}

				/* insert new brand */
				brandsModel.insertBrand(brandData,function(error, brand){

					/* checks if the user logged is an Admin */
					if(req.user.role == 'Admin'){

						var object ={
							'brands_id_brands':brand.insertId,
							'users_id_users':req.user.id,
							'roles_id_role': req.user.id_role
						};

						/* insert the user to the brand */
						userBrandsModel.insertUserBrand(object,function(error, usersBrand){

						});
					}

					/* gets the brand inserted */
					brandsModel.getBrand(brand.insertId,function(error, newBrand){

						res.send(newBrand);
					});
				});
			}
		});
	}else{

		var error = {
			'auth' : "is not authenticated"
		};

		res.send(error);
	}
}


/* this funtion updates the status of the brand */
function updateBrandStatus (req,res){

	/* checks if there is someone authenticated */
	if(req.isAuthenticated()){

		var brand = req.body;

		/* update the status of the brand */
		brandsModel.updateStatus(brand,function(err,message){
			res.send(message);
		});
	}else{

		var error = {
			'auth' : "is not authenticated"
		};

		res.send(error);
	}
}

/* this function gets all team of a brand */
function getTeam(req,res){

	var id_brand = req.params.id;

	/* gets users brands by id brnad */
	userBrandsModel.getUserBrandsById(id_brand,function(error, usersBrands){

		if(usersBrands.length > 0){

			/* gets brand by id brand */
			brandsModel.getBrand(id_brand,function(error, brand){
				
				/* checks if brand selected exist in the table user_brands */
				if(usersBrands.length >= 1){

						/* gets all users brands that the user logged administers */
						userBrandsModel.getBrandUser(req.user.id,function(error, userBrandsLogged){

							/* search if the user logged administers the brand selected */
							var resFind = userBrandsLogged.find(function(userBrand){
								return (userBrand.brands_id_brands === brand[0].id_brands);
							});

							/* checks if the user administers the brand selected or if the user logged is a SurperAdmin*/
							if(resFind || req.user.role === "SuperAdmin"){

								var usersIds=[];

								/* push all users of the brand selected */
								for (var i = 0, len = usersBrands.length; i < len; i++) {
									usersIds.push(usersBrands[i].users_id_users);
								}

								/* checks if there are one or more users in the brand selected */
								if(usersIds.length > 0){

									/* gets all users than do match with the usersIds */
									usersModel.getUserByIds(usersIds,function(err,users){

										/* gets all social networks for each user */
										userNetworksModel.getUserNetworksByIds(usersIds,function(err,socialNetworks){

											/* join all social network for each user*/
											var newusers = AddSocialNetworks(socialNetworks,users);

											var myBrand = {
												'id_brand' : id_brand,
												'brandName': brand[0].name,
												'brandIsEnable': brand[0].isEnable
											};

											var data = {
												'users':newusers,
												'brand':myBrand
											}

											/* render to team and send all users for that team */
											res.render('team',data);
										});
									});
								}else{

									var myBrand = {
										'id_brand' : id_brand,
										'brandName' : brand[0].name,
										'brandIsEnable' : brand[0].isEnable
									};

									var data = {
										'brand':myBrand
									}

									/* render to team witout users */
									res.render('team',data);
								}
							}else{

								/* redirect to admin */
								res.redirect("/admin");
							}
						});
					
				}else{

					var myBrand = {
						'id_brand' : id_brand,
						'brandName' : brand[0].name,
						'brandIsEnable' : brand[0].isEnable
					};

					var data = {
						'brand':myBrand
					}

					/* render to team and send all users for that team */
					res.render('team',data);
				}
			});
		}else{
			/* redirect to admin */
			res.redirect("/admin");
		}
	});
}

/* this function add all social networks for each user */
function AddSocialNetworks(socialNetworks,users){

	var social_networks=[];
	var number = 0;
	var sameUser = false;

	for (var i = 0; i < users.length; i++) {

		for (var c = 0; c < socialNetworks.length; c++) {

			if(users[i].id_users == socialNetworks[c].id_users){

				if(socialNetworks[c].name == 'facebook'){
					users[i].facebook = {"name": socialNetworks[c].user_name, "id": socialNetworks[c].id_social_networks};
				}

				if(socialNetworks[c].name=='twitter'){
					users[i].twitter = {"name": socialNetworks[c].user_name, "id": socialNetworks[c].id_social_networks};

				}

				if(socialNetworks[c].name=='linkedin'){
					users[i].linkedin = {"name": socialNetworks[c].user_name, "id": socialNetworks[c].id_social_networks};
				}

				if(socialNetworks[c].name=='instagram'){
					users[i].instagram = {"name": socialNetworks[c].user_name, "id": socialNetworks[c].id_social_networks};
				}

				if(socialNetworks[c].name=='skype'){
					users[i].skype = {"name": socialNetworks[c].user_name, "id": socialNetworks[c].id_social_networks};
				}
			}
		}
	}
	return users;
}

module.exports = router;
