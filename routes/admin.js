//EXPRESS
var express = require('express');
var router = express.Router();
var multer  = require('multer');

//PASSPORT
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;

//ENCRIPT
var bcrypt = require('bcrypt-nodejs');

//MODELS
var socialNetworksModel = require('../models/socialNetworksModel');
var userNetworksModel = require('../models/userNetworksModel');
var userBrandsModel = require('../models/userBrandsModel');
var bigAccountsModel = require('../models/bigAccountsModel');
var rolesModel = require('../models/rolesModel');
var brandsModel = require('../models/brandsModel');
var usersModel = require('../models/usersModel');

//AUTHTENTICATE
var authenticated = require('./authenticated');

//PATHCONFIG
var path_config = require('./path_config');

//ROUTES

/* Route render to the admin view */
router.get('/admin', authenticated.ensureAuthenticated, showsAdminView);

/* Route render to the login view */
router.get('/admin/login',authenticated.isNotAuthenticated, showLoginView);

/* Route render to the register view */
router.get('/register/:id_brand', showRegisterView);

/* Route to register new user */
router.post('/register',registerNewUser);

/* Route to logout */
router.get('/logout',logout);


/* This function move the image seleted to another path */
var storage = multer.diskStorage({

	/* destination where the image are going be saved */
	destination: function (request, file, callback) {

		/* gets brand */
		brandsModel.getBrand(request.body.id_brand,function(err, brand) {

			/* checks if the brand exist */
			if(brand.length > 0){

				/* gets the user by his number */
				usersModel.getUser(request.body.number, function(err, user) {

					/* checks if the user is not register */
					if(user.length == 0){

						callback(null, path_config.imagesFolders.team);

					}else{

						/* send error */
						callback('UserError',new Error());
					}
				});

			}else{

				/* send error */
				callback('BrandError',new Error());
			}
		});
	},

	filename: function (request, file, callback) {

		var valid = false;

		/* this function checks if the file selected is valid or not */
		valid = isValidImage(file.mimetype);

		/* checks if the image is valid and if the field name is not empty */
		if(valid && request.body.number){

			/* get the full current date */
			var datetime = getFullDate();

			/* get the mimetype of the image selected */
			var mimetypeReverse = getMimetype(file.originalname);

			/* save the mimetype to the request body */
			request.body.newMimeType = mimetypeReverse;

			/* update the image name with the user details */
			file.originalname = request.body.number+"-"+datetime+"."+mimetypeReverse;

			/* move the image selected to the destination path */
			callback(null, file.originalname);

		}else{

			/* show and error if the file is not a image or the user has an empty number */
			callback('FileError',new Error())
		}
	}
});


/* get the full current date */
function getFullDate(){

	date = new Date();
	var year = date.getFullYear();
	var month = date.getMonth();
	var day = date.getDay();

	var hour = date.getHours();
	var minutes = date.getMinutes();
	var seconds = date.getSeconds();

	var fullDate = year+":"+month+":"+day+":"+hour+":"+minutes+":"+seconds;
	return fullDate;
}

/* this function check if the file is a allow image */
function isValidImage(mimetype){
	var listValidImages = ['image/jpeg','image/png','image/jpg'];

	for(var i = 0; i < listValidImages.length; i++ ){

		if(listValidImages[i] == mimetype){

			return true;
		}
	}

	return false;
}

/* this function get the mimetype */
function getMimetype(originalname){
	var mimetype = '';

	for (var i = originalname.length-1; i >= 0; i--) {

		if(originalname[i] == '.'){
			break;
		}

		mimetype += originalname[i];
	}

	var mimetypeReverse = mimetype.split("").reverse().join('');

	return mimetypeReverse;
}

/* this function add one by one social netwok of the user */
function addSocialNeworks(socialNetworks,UserId){

	/* gets all social networks */
	socialNetworksModel.getsocialNetwoks(function(err,allSocialNetwoks){
		var redSocial  = {};
		var isUndefined = true;

		/* runs social networks of the user inserted */
		for (var i = 0; i < socialNetworks.length; i++) {

			if (socialNetworks[i].facebook !== undefined) {
				isUndefined = false;
				redSocial.name = 'facebook';
				redSocial.userName = socialNetworks[i].facebook;
			}
			if (socialNetworks[i].twitter !== undefined) {
				isUndefined = false;
				redSocial.name = 'twitter';
				redSocial.userName = socialNetworks[i].twitter;
			}
			if (socialNetworks[i].linkedin !== undefined) {
				isUndefined = false;
				redSocial.name = 'linkedin';
				redSocial.userName = socialNetworks[i].linkedin;
			}
			if (socialNetworks[i].instagram !== undefined) {
				isUndefined = false;
				redSocial.name = 'instagram';
				redSocial.userName = socialNetworks[i].instagram;
			}
			if (socialNetworks[i].skype !== undefined) {
				isUndefined = false;
				redSocial.name = 'skype';
				redSocial.userName = socialNetworks[i].skype;
			}

			/* checks if the social networks is found */
			if (!isUndefined) {

				for (var c = 0; c < allSocialNetwoks.length; c++) {

					/* ones the social networks id found finds which does match with the database */
					if(allSocialNetwoks[c].name == redSocial.name ){

						var UserNetworkData ={
							'social_networks_id_social_networks' : allSocialNetwoks[c].id_social_networks,
							'id_users' : UserId,
							'user_name' : redSocial.userName 
						}

						/* insert new social network for an user inserted */
						userNetworksModel.insertUserNetworks(UserNetworkData,function(err,mySocialNetworks){

						});
					}
				}

				isUndefined = true;
				redSocial.name = '';
				redSocial.userName = '';
			}
		}
	});
}

/* moves the image selected to another path */
var upload = multer({ storage: storage }).single("avatar");

/* this function shows the admin view */
function showsAdminView(req,res){

	/* checks if the user logged is a SuperAdmin */
	if(req.user.role == 'SuperAdmin'){

		/* gets all big accounts */
		bigAccountsModel.getAccounts(function(err, accounts) {

			/* render the admin view */
			res.render('admin',{ accounts });
		});

	}else{

		/* gets the brand of the user logged */
		brandsModel.getBrand(req.user.user_brands,function(err, brand) {

			/* redirect to the brand view */
			res.redirect('brands/'+brand[0].id_big_accounts);
		});
	}
}

/* this function render to the login view */
function showLoginView(req,res){

	/* render the login view */
	res.render('login');
}

/* this function shows the register view */
function showRegisterView(req,res){
	res.render('register');
}

/* this function registers new users */
function registerNewUser(req,res){

	/* move the image to the destination path */
	upload(req, res, function (err) {

		/* gets brand */
		brandsModel.getBrand(req.body.id_brand,function(err, brand) {

			/* if the user don't load the image the number must to be checked again */
			usersModel.getUser(req.body.number, function(error, user) {

				/* checks if the brand exist */
				if(brand.length <= 0){

					err = "BrandError";

				}else{

					/* checks if the user doesn't exist  */
					if(user.length != 0){

						err = "UserError"; 
					}
				}

				/* this is to check if the there was any error moving the image or image not allowed */
				if (err) {
					var error ={}

					if(err == "BrandError"){

						error = "The brand selected doesn't exit";

					}else{

						if(err == "UserError"){

							error = "The user already exit";

						}else{

							error = "The avatar must be an image file";
						}
					}

					res.send({'error':error});

				}else{

					/* this is to checks if the image file was selected */
					if(req.file){

						/* split the image by "build/" */
						var path = req.file.path.split('build/');

						req.body.avatar = '/' + path[1];

					}else{

						/* add default image for the new user fo the team */
						req.file = path_config.imagesFolders.defaultUser;

						req.body.avatar = req.file;
					}

					var userData = {
						'name' : req.body.name,
						'lastname' : req.body.lastName,
						'number' : req.body.number,
						'avatar' : req.body.avatar,
						'active': false,
						'password' : ''
					}

					var socialNetworks = [];
					socialNetworks.push({"facebook":req.body.facebook});
					socialNetworks.push({"twitter":req.body.twitter});
					socialNetworks.push({"linkedin":req.body.linkedin});
					socialNetworks.push({"instagram":req.body.instagram});
					socialNetworks.push({"skype":req.body.skype});

					/* insert the new user */
					usersModel.insertUser(userData, function(err, user) {

						var object ={
						'brands_id_brands':req.body.id_brand,
						'users_id_users':user.insertId,
						'roles_id_role': '3'
						};

						/* add his or her social networks */
						addSocialNeworks(socialNetworks,user.insertId);

						/* add the user inserted to the brand */
						userBrandsModel.insertUserBrand(object,function(error, usersBrand){

							req.body.id_users = user.insertId

							res.send({'success': 'You was added successfully to the team!'});
						});
					});
				}
			});
		});
	});
}

/* 
* redirect to admin if the the authentication was success
* or redirect to login if was not
*/
router.post('/admin/login',

	passport.authenticate('local', { 
		successRedirect: '/admin',
		failureRedirect: '/admin/login',
		failureFlash: true 
	})
);

/* passport configuration */
passport.use(new LocalStrategy(

	function(username, password, done) {

		/* Finds the user buy her/his number */
		usersModel.getUser(username, function(err, user) {

			/* If there is any error the authentication process ends */
			if (err) {
				return done(err);
			}
			/* If the the user is not found go to login view with an error */
			if (!user || user.length <= 0) {
				
				return done(null, false, { message: 'Incorrect username.' });
			
			}else{

				/* check the password inserted with the encript password */
				bcrypt.compare(password, user[0].password, function(err, res) {

					/* If everything is ok, checks one more time the number of the user */
					if(res && user[0].number == username ){

						/* gets the user brand to know witch is the role of that user */
						userBrandsModel.getBrandUser(user[0].id_users,function(error, userBrands){

							/* gets the the name of the role */
							rolesModel.getRoleUser(userBrands[0].roles_id_role,function(error, role){

								if(role[0].name == "SuperAdmin" || role[0].name == "Admin"){

									/* object with the information of the user that will be authenticated */
									var authUser = {
										'id' : user[0].id_users,
										'name' : user[0].name,
										'number' : user[0].number,
										'role':role[0].name,
										'id_role':role[0].id_role,
										'user_brands':userBrands[0].brands_id_brands
									}

									if(role[0].name == "SuperAdmin"){
										authUser.superAdmin = role[0].name;
									}
									
									/* authenticates the user */
									return done(null, authUser);
								}else{
									
									/* if the user is not an admin or superadmin will be not authenticated */
									return done(null, false, { message: "you don't have permitions to get in" });
								}
							});
						});
					}else{
						/* show and error message */
						return done(null, false, { message: 'Incorrect Password.' });
					}
				});
			}	
		});
	}
));

/* passport configuration */
passport.serializeUser(function(user, done) {
	done(null, user);
});

/* passport configuration */
passport.deserializeUser(function(user, done) {
	done(null, user);
});

/* ends passport configuration */


/* this function logout the user */
function logout(req,res){

	req.logout();
	req.flash('success_msg','you are logged out');

	res.redirect('/admin/login');

}

module.exports = router;
