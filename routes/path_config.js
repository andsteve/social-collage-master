module.exports = {
    appFolder: './app',
    buildFolder: './build',
    imagesFolders: {
        brands: './build/assets/images/logos/brands',
        big_counts: './build/assets/images/logos/big_counts',
        team: 'build/assets/images/employees',
        defaultUser:'/assets/images/employees/user.jpg',
        userAvatar: '/assets/images/employees'
    }
};