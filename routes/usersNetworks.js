//EXPRESS
var express = require('express');
var router = express.Router();

//PATH CONFIG
var path_config = require('./path_config');

//MODELS
var userNetworksModel = require('../models/userNetworksModel');

//AUTHTENTICATED
var authenticated = require('./authenticated');

//ROUTES
router.post('/addUsersNetwork', addUsersNetwork );

/* this function add new user network */
function addUsersNetwork(req, res, next){
	
	/* checks if there is someone authenticated */
	if(req.isAuthenticated()){
		
		/* create the object users networks */
		var users_networks = {
			'id_user' : req.body.id_users,
			'id_social': req.body.social_networks_id_social_networks
		}

		/* review if the social network and user already exist */
		userNetworksModel.getUserNetwork(users_networks,function(err, userNetwork) {

			/* checks if the userNetwork has one element */
			if(userNetwork.length == 0){

				/* insert the new user social network */
				userNetworksModel.insertUserNetworks(req.body,function(err, userNetworkId) {

					/* get the new user social network added */
					userNetworksModel.getUserNetworkByIdJoined(users_networks,function(err, userNetworkInserted) {

						/* send the new user social network */
						res.send(userNetworkInserted);
					});
				});

			}else{
				/* send the message error */
				res.send({"msg":"already exist"});
			}
		});

	}else{

		var error = {
			'auth' : "is not authenticated"
		};
		
		res.send(error);
	}
}

module.exports = router;
