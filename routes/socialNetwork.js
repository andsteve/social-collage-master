//EXPRESS
var express = require('express');
var router = express.Router();

//MODELS
var socialNetworksModel = require('../models/socialNetworksModel');

//PATHCONFIG
var path_config = require('./path_config');

//AUTHTENTICATE
var authenticated = require('./authenticated');

//ROUTES

/* Route to render the profile of the specific user */
router.get('/getSocialNetwork', getSocialNetwork);

function getSocialNetwork(req, res, next) {

	/* checks if there is someone authenticated */
	if(req.isAuthenticated()){
		
		/* this function call all the social networks of the database */
		socialNetworksModel.getsocialNetwoks(function(err, SocialNetworks) {
			
			/* send all social networks */
			res.send(SocialNetworks);
		});

	}else{
		
		var error = {
			'auth' : "is not authenticated"
		};

		res.send(error);
	}

}

module.exports = router;