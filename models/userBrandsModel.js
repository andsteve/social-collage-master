//conect date base
var connection = require('./DBconeccion');

//create objeto to save all
var userBrandModel = {};

//get the specific users brands by id
userBrandModel.getBrandUser = function(id_user,callback)
{
	if (connection) 
	{
		var sql = 'SELECT * FROM user_brands WHERE users_id_users = ' + connection.escape(id_user);
		connection.query(sql, function(error, row) 
		{
			if(error)
			{
				throw error;
			}
			else
			{
				callback(null, row);
			}
		});
	}
}

userBrandModel.getBrandJoinedBrandUser = function(id_user,callback)
{
	if (connection) 
	{
	 		var sql = "SELECT "+
					
					"ub.brands_id_brands, "+

					"br.name, "+
					"br.id_big_accounts "+

					"FROM user_brands as ub "+
					"inner join brands as br on "+
					"ub.brands_id_brands = br.id_brands "+

					"WHERE ub.users_id_users = " + connection.escape(id_user) + 
					
					" ORDER BY ub.users_id_users";
		connection.query(sql, function(error, row) 
		{
			if(error)
			{
				throw error;
			}
			else
			{
				callback(null, row);
			}
		});
	}
}





userBrandModel.getUserBrandsById = function(id_brand,callback)
{
	if (connection) 
	{
		var sql = 'SELECT users_id_users FROM user_brands WHERE brands_id_brands = ' + connection.escape(id_brand);
		connection.query(sql, function(error, row) 
		{
			if(error)
			{
				throw error;
			}
			else
			{
				callback(null, row);
			}
		});
	}
}

// add new user brand
userBrandModel.insertUserBrand = function(userBrandData,callback)
{
	if (connection) 
	{
		connection.query('INSERT INTO user_brands SET ?', userBrandData, function(error, userBrand) 
		{
			if(error)
			{
				throw error;
			}
			else
			{
				//get back the lastone user
				callback(null,userBrand);
			}
		});
	}
}

//update user
userBrandModel.updateUserBrand = function(userBrandData, callback)
{
	if(connection)
	{
		var sql = 'UPDATE user_brands SET brands_id_brands = ' + connection.escape(userBrandData.brands_id_brands)+
		' WHERE users_id_users = ' + userBrandData.id_users;

		connection.query(sql, function(error, result) 
		{
			if(error)
			{
				throw error;
			}
			else
			{
				callback(null,{"msg":"User was reassigned successfully"});
			}
		});
	}
}
//update user
userBrandModel.updateRole = function(userBrandData, callback)
{
	if(connection)
	{
		var sql = 'UPDATE user_brands SET roles_id_role = ' + connection.escape(userBrandData.id_role)+
		' WHERE users_id_users = ' + userBrandData.id_user;

		connection.query(sql, function(error, result) 
		{
			if(error)
			{
				throw error;
			}
			else
			{
				callback(null,{"msg":"Role updated successfully"});
			}
		});
	}
}

userBrandModel.deleteUserBrands = function(id,callback)
{
	if(connection)
	{
		var sqlExists = 'SELECT * FROM user_brands WHERE users_id_users = ' + connection.escape(id);
		connection.query(sqlExists, function(err, row) 
		{
			if(row)
			{
				var sqlDeleteUserBrands = 'DELETE FROM user_brands WHERE users_id_users = ' + connection.escape(id);

				connection.query(sqlDeleteUserBrands, function(error, result) 
				{
					if(error)
					{
						throw error;
					}
					else
					{
						callback(null,{"msg":true});
					}
				});
			}
			else
			{
				callback(null,{"msg":true});
			}
		});
	}
}

module.exports = userBrandModel;