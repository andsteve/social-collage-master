var mysql      = require('mysql');

var connection = mysql.createConnection({
	host     : '127.0.0.1',
	user     : 'root',
	password : 'root',
	database : 'social_collage'
});

// var connection = mysql.createConnection({
// 	host     : 'localhost',
// 	user     : 'DB_PDGPortrait',
// 	password : 'rgHf4J91ts8KVwCg',
// 	database : 'DB_PDGPortrait'
// });


connection.connect(function(err){
	if(!err) {
		console.log("Database is connected ... \n\n");  
	} else {
		console.log("Error connecting database ... \n\n");
	}
});

module.exports = connection;


