var connection = require('./DBconeccion');
//create objeto to save all
var brandsModel = {};
 
brandsModel.getAccountsOfBigAccount = function(id_big_accounts,callback)
{
	if (connection) 
	{
		var sql = 'SELECT * FROM brands WHERE id_big_accounts = ' + connection.escape(id_big_accounts);
		connection.query(sql, function(error, row) 
		{
			if(error)
			{
				throw error;
			}
			else
			{
				callback(null, row);
			}
		});
	}
}

brandsModel.getAccountsActivesOfBigAccount = function(id_big_accounts,callback)
{
	if (connection) 
	{
		var sql = 'SELECT * FROM brands WHERE id_big_accounts = ' + connection.escape(id_big_accounts)
		+ " AND isEnable = true ";
		connection.query(sql, function(error, row) 
		{
			if(error)
			{
				throw error;
			}
			else
			{
				callback(null, row);
			}
		});
	}
}

brandsModel.getAccountsByBrandIds = function(arrayIds,callback)
{
	if (connection) 
	{
		var sql = "SELECT * FROM brands WHERE id_brands in ("+arrayIds+")";
		connection.query(sql, function(error, rows) 
		{
			if(error)
			{
				throw error;
			}
			else
			{
				callback(null, rows);
			}
		});
	}
}

brandsModel.getAccountsActiveByUserIds = function(arrayIds,callback)
{
	if (connection) 
	{
		var sql = "SELECT * FROM brands WHERE id_brands in ("+arrayIds+") AND isEnable = true ";
		connection.query(sql, function(error, rows) 
		{
			if(error)
			{
				throw error;
			}
			else
			{
				callback(null, rows);
			}
		});
	}
}

brandsModel.getBrand = function(brand_id,callback)
{
	if (connection) 
	{
		var sql = 'SELECT * FROM brands WHERE id_brands = ' + connection.escape(brand_id);
		connection.query(sql, function(error, row) 
		{
			if(error)
			{
				throw error;
			}
			else
			{
				callback(null, row);
			}
		});
	}
}

brandsModel.insertBrand = function(brandData,callback)
{
	if (connection) 
	{
		connection.query('INSERT INTO brands SET ?', brandData, function(error, result) 
		{
			if(error)
			{
				throw error;
			}
			else
			{

				callback(null,{"insertId" : result.insertId});
			}
		});
	}
}

//update brand status
brandsModel.updateStatus = function(brandData, callback)
{
	if(connection)
	{
		var sql = 'UPDATE brands SET isEnable = ' + connection.escape(brandData.active)+
		' WHERE id_brands = ' + brandData.id_brands;

		connection.query(sql, function(error, result) 
		{
			if(error)
			{
				throw error;
			}
			else
			{
				callback(null,{"msg":"Status updated successfully"});
			}
		});
	}
}

module.exports = brandsModel;