//conect date base
var connection = require('./DBconeccion');

//create objeto to save all
var roleModel = {};
//get user by id
roleModel.getRoleUser = function(id_role,callback)
{
	if (connection) 
	{
		var sql = 'SELECT * FROM roles WHERE id_role = ' + connection.escape(id_role);
		connection.query(sql, function(error, row) 
		{
			if(error)
			{
				throw error;
			}
			else
			{
				callback(null, row);
			}
		});
	}
}


//get all roless
roleModel.getRoles = function(callback)
{
	if (connection) 
	{
		connection.query('SELECT * FROM roles ORDER BY id_role', function(error, rows) {
			if(error)
			{
				throw error;
			}
			else
			{
				callback(null, rows);
			}
		});
	}
}
module.exports = roleModel;