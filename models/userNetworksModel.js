//CONNECTION
var connection = require('./DBconeccion');

//USER_NETWORKS_MODEL_OBJECT
var userNetworksModel = {};

/* gets users network that do match with id user and id social network */
userNetworksModel.getUserNetwork = function(userNetwork,callback)
{
	if (connection) 
	{
		var sql = 'SELECT * FROM users_networks WHERE id_users = ' + connection.escape(userNetwork.id_user) +
		' AND social_networks_id_social_networks = ' + connection.escape(userNetwork.id_social);
		connection.query(sql, function(error, row) 
		{
			if(error)
			{
				throw error;
			}
			else
			{
				callback(null, row);
			}
		});
	}
}

/* gets user network and social network by id_users and id_social_networks */
userNetworksModel.getUserNetworkByIdJoined = function(userNetwork,callback)
{
	if (connection) 
	{
		var sql = "SELECT "+

					"sn.name, "+
					"sn.id_social_networks, "+
					"sn.link, "+
					"sn.image, "+

					"un.id_users, "+
					"un.user_name, "+
					"un.social_networks_id_social_networks "+

					"FROM users_networks as un "+
					"INNER JOIN social_networks as sn on "+
					"sn.id_social_networks = un.social_networks_id_social_networks "+

					"WHERE un.id_users = " + connection.escape(userNetwork.id_user) + " AND " +
 					"un.social_networks_id_social_networks = " + connection.escape(userNetwork.id_social) +
					
					" ORDER BY un.id_users";

		connection.query(sql, function(error, rows) 
		{
			if(error)
			{
				throw error;
			}
			else
			{
				callback(null, rows);
			}
		});
	}
}

/* gets user network and social network by list of id_users */
userNetworksModel.getUserNetworksByIds = function(arrayIds,callback)
{
	if (connection) 
	{
		var sql = "SELECT "+

					"sn.name, "+
					"sn.id_social_networks, "+
					"sn.link, "+
					"sn.image, "+

					"un.id_users, "+
					"un.user_name, "+
					"un.social_networks_id_social_networks "+

					"FROM users_networks as un "+
					"INNER JOIN social_networks as sn on "+
					"sn.id_social_networks = un.social_networks_id_social_networks "+

					"WHERE un.id_users in ("+arrayIds+") "+
					"ORDER BY un.id_users";

		connection.query(sql, function(error, rows) 
		{
			if(error)
			{
				throw error;
			}
			else
			{
				callback(null, rows);
			}
		});
	}
}


/* add new user nerwork */
userNetworksModel.insertUserNetworks = function(userNetworkData,callback)
{	
	if (connection) 
	{
		connection.query('INSERT INTO users_networks SET ?', userNetworkData, function(error, result) 
		{
			if(error)
			{
				throw error;
			}
			else
			{
				//get back the lastone user
				callback(null,{"insertId" : result.insertId});
			}
		});
	}
}

/* update user nerwork */
userNetworksModel.updateUserNetwork = function(userNetwork, callback)
{
	if(connection)
	{
		var sql = 'UPDATE users_networks SET user_name = ' + connection.escape(userNetwork.user_name) + 
		' WHERE id_users = ' + userNetwork.id_user + ' AND ' +
		'social_networks_id_social_networks = ' + userNetwork.id_social;
 
		connection.query(sql, function(error, result) 
		{
			if(error)
			{
				throw error;
			}
			else
			{
				callback(null,{"msg":"user name updated successfully"});
			}
		});
	}
}

/* delete user nerwork */
userNetworksModel.deleteUserNetwork = function(userNetwork,callback)
{
	if(connection)
	{
		var sqlExists = 'SELECT * FROM users_networks WHERE id_users = ' + connection.escape(userNetwork.id_user) +
						' AND social_networks_id_social_networks = ' + connection.escape(userNetwork.id_social);
		
		connection.query(sqlExists, function(err, row) 
		{
			//checks if the user_brands id exist
			if(row)
			{
				var sqlDeleteUserBrands = 'DELETE FROM users_networks WHERE id_users = ' + connection.escape(userNetwork.id_user)+
						' AND social_networks_id_social_networks = ' + connection.escape(userNetwork.id_social);

				connection.query(sqlDeleteUserBrands, function(error, result) 
				{
					if(error)
					{
						throw error;
					}
					else
					{
						callback(null,{"msg":"social network deleted successfully"});
					}
				});
			}
			else
			{
				callback(null,{"msg":true});
			}
		});
	}
}

/* delete all user nerwork of the one user */
userNetworksModel.deleteUserNetworskByIdUser = function(id,callback)
{
	if(connection)
	{
		var sqlExists = 'SELECT * FROM users_networks WHERE id_users = ' + connection.escape(id);
		connection.query(sqlExists, function(err, row) 
		{

			if(row)
			{
				var sqlDeleteUsersNetworks = 'DELETE FROM users_networks WHERE id_users = ' + connection.escape(id);

				connection.query(sqlDeleteUsersNetworks, function(error, result) 
				{
					if(error)
					{
						throw error;
					}
					else
					{
						callback(null,{"msg":true});
					}
				});
			}
			else
			{
				callback(null,{"msg":true});
			}
		});
	}
}

module.exports = userNetworksModel;


