//CONNECTION
var connection = require('./DBconeccion');

//USER_MODEL_OBJECT
var userModel = {};
 
/* gets all users */
userModel.getUsers = function(callback)
{
	if (connection) 
	{
		connection.query('SELECT * FROM users ORDER BY id_users', function(error, rows) {
			if(error)
			{
				throw error;
			}
			else
			{
				callback(null, rows);
			}
		});
	}
}
 
/* gets user by his number */
userModel.getUser = function(user_number,callback)
{
	if (connection) 
	{
		var sql = 'SELECT * FROM users WHERE number = ' + connection.escape(user_number);
		connection.query(sql, function(error, row) 
		{
			if(error)
			{
				throw error;
			}
			else
			{
				callback(null, row);
			}
		});
	}
}

/* gets user by his id */
userModel.getUserById = function(user_id,callback)
{
	if (connection) 
	{
		var sql = 'SELECT * FROM users WHERE id_users = ' + connection.escape(user_id);
		connection.query(sql, function(error, row) 
		{
			if(error)
			{
				throw error;
			}
			else
			{
				callback(null, row);
			}
		});
	}
}

/* gets all users that do match with a list of id users */
userModel.getUserByIds = function(arrayIds,callback)
{
	if (connection) 
	{
		var sql = "SELECT * FROM users WHERE id_users in ("+arrayIds+")";
		connection.query(sql, function(error, rows) 
		{
			if(error)
			{
				throw error;
			}
			else
			{
				callback(null, rows);
			}
		});
	}
}

/* gets all users that do match with a list of id users and are active TRUE */
userModel.getUserActivesByIds = function(arrayIds,callback)
{
	if (connection) 
	{
		var sql = "SELECT * FROM users WHERE id_users in ("+arrayIds+") AND active = true";
		connection.query(sql, function(error, rows) 
		{
			if(error)
			{
				throw error;
			}
			else
			{
				callback(null, rows);
			}
		});
	}
}


/* add new user */
userModel.insertUser = function(userData,callback)
{
	if (connection) 
	{
		connection.query('INSERT INTO users SET ?', userData, function(error, result) 
		{
			if(error)
			{
				throw error;
			}
			else
			{
				callback(null,{"insertId" : result.insertId});
			}
		});
	}
}

/* delete a specific user */
userModel.deleteUser = function(id, callback)
{
	if(connection)
	{
		var sqlExists = 'SELECT * FROM users WHERE id_users = ' + connection.escape(id);
		connection.query(sqlExists, function(err, row) 
		{
			/* checks if the user exist */
			if(row)
			{
				var sqlDeleteUser = 'DELETE FROM users WHERE id_users = ' + connection.escape(id);
				
				connection.query(sqlDeleteUser, function(error, result) 
				{
					if(error)
					{
						throw error;
					}
					else
					{
						callback(null,{"msg":"User deleted successfully"});
					}
				});
			}
			else
			{
				callback(null,{"msg":"notExist"});
			}
		});
	}
}

/* update personal information of the user */
userModel.updateUser = function(userData, callback)
{
	if(connection)
	{
		var sql = 'UPDATE users SET name = ' + connection.escape(userData.name) + ',' +  
		'lastname = ' + connection.escape(userData.lastname) +
		'WHERE id_users = ' + userData.id_users;
 
		connection.query(sql, function(error, result) 
		{
			if(error)
			{
				throw error;
			}
			else
			{
				callback(null,{"msg":"User updated successfully"});
			}
		});
	}
}


/* update avatar of the user */
userModel.updateUserAvatar = function(userData, callback)
{
	if(connection)
	{
		var sql = 'UPDATE users SET avatar = ' + connection.escape(userData.avatar)+
		'WHERE id_users = ' + userData.id_user;
 
		connection.query(sql, function(error, result) 
		{
			if(error)
			{
				throw error;
			}
			else
			{
				callback(null,{"msg":"Avatar updated successfully"});
			}
		});
	}
}


/* update status of the user  */
userModel.updateStatus = function(userData, callback)
{
	if(connection)
	{
		var sql = 'UPDATE users SET active = ' + connection.escape(userData.active)+
		' WHERE id_users = ' + userData.id_users;

		connection.query(sql, function(error, result) 
		{
			if(error)
			{
				throw error;
			}
			else
			{
				callback(null,{"msg":"Status updated successfully"});
			}
		});
	}
}

/* update passworks of the user */
userModel.updatePassword = function(userData, callback)
{
	if(connection)
	{
		var sql = 'UPDATE users SET password = ' + connection.escape(userData.password)+
		' WHERE id_users = ' + userData.id_users;

		connection.query(sql, function(error, result) 
		{
			if(error)
			{
				throw error;
			}
			else
			{
				callback(null,{"msg":"Status updated successfully"});
			}
		});
	}
}

/* export userModel */
module.exports = userModel;
 
