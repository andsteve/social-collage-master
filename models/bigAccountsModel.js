var connection = require('./DBconeccion');
//create objeto to save all
var bigAccountsModel = {};
 
//get all big accounts
bigAccountsModel.getAccounts = function(callback)
{
	if (connection) 
	{
		connection.query('SELECT * FROM big_accounts ORDER BY id_big_accounts', function(error, rows) {
			if(error)
			{
				throw error;
			}
			else
			{
				callback(null, rows);
			}
		});
	}
}

//get all big accounts that are active
bigAccountsModel.getAccountsActive = function(callback)
{
	if (connection) 
	{
		connection.query('SELECT * FROM big_accounts WHERE active = true ', function(error, rows) {
			if(error)
			{
				throw error;
			}
			else
			{
				callback(null, rows);
			}
		});
	}
}


// //get big account by id
bigAccountsModel.getBigAccount = function(id_big_accounts,callback)
{
	if (connection) 
	{
		var sql = 'SELECT * FROM big_accounts WHERE id_big_accounts = ' + connection.escape(id_big_accounts);
		connection.query(sql, function(error, row) 
		{
			if(error)
			{
				throw error;
			}
			else
			{
				callback(null, row);
			}
		});
	}
}

bigAccountsModel.insertAccount = function(accountsData,callback)
{
	if (connection) 
	{
		connection.query('INSERT INTO big_accounts SET ?', accountsData, function(error, result) 
		{
			if(error)
			{
				throw error;
			}
			else
			{
				//get back the lastone user
				callback(null,{"insertId" : result.insertId});
			}
		});
	}
}

//update big account status
bigAccountsModel.updateStatus = function(bigAccountData, callback)
{
	if(connection)
	{
		var sql = 'UPDATE big_accounts SET active = ' + connection.escape(bigAccountData.active)+
		' WHERE id_big_accounts = ' + bigAccountData.id_big_account;

		connection.query(sql, function(error, result) 
		{
			if(error)
			{
				throw error;
			}
			else
			{
				callback(null,{"msg":"Status updated successfully"});
			}
		});
	}
}


module.exports = bigAccountsModel;
















